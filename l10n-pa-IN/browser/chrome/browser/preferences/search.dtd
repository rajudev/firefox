<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY searchBar.label                       "ਖੋਜ ਪੱਟੀ">

<!ENTITY searchBar.hidden.label                "ਸਿਰਨਾਵਾਂ ਪੱਟੀ ਨੂੰ ਖੋਜ ਅਤੇ ਨੇਵੀਗੇਸ਼ਨ ਲਈ ਵਰਤੋਂ">
<!ENTITY searchBar.shown.label                 "ਟੂਲਬਾਰ 'ਚ ਖੋਜ ਪੱਟੀ ਜੋੜੋ">

<!-- LOCALIZATION NOTE (showSearchSuggestionsAboveHistory.label): This string
     describes what the user will observe when the system prioritizes search
     suggestions over browsing history in the results that extend down from the
     address bar. In the original English string, "ahead" refers to location
     (appearing most proximate to), not time (appearing before). -->
<!ENTITY showSearchSuggestionsAboveHistory.label "ਸਿਰਨਾਵਾਂ ਪੱਟੀ ਨਤੀਜਿਆਂ ਵਿੱਚ ਬਰਾਊਜ਼ ਕਰਨ ਦੇ ਅਤੀਤ ਤੋਂ ਪਹਿਲਾਂ ਹੀ ਖੋਜ ਸੁਝਾਅ ਵੇਖਾਓ">

<!ENTITY defaultSearchEngine.label             "ਮੂਲ ਖੋਜ ਇੰਜਣ">

<!ENTITY chooseYourDefaultSearchEngine2.label   "ਸਿਰਨਾਵਾਂ ਪੱਟੀ ਅਤੇ ਖੋਜ ਪੱਟੀ 'ਚ ਵਰਤਣ ਲਈ ਡਿਫਾਲਟ ਖੋਜ ਇੰਜਣ ਨੂੰ ਚੁਣੋ।">

<!ENTITY provideSearchSuggestions.label        "ਖੋਜ ਸੁਝਾਅ ਦਿੰਦਾ ਹੈ">
<!ENTITY provideSearchSuggestions.accesskey    "s">

<!ENTITY showURLBarSuggestions2.label           "ਸਿਰਨਾਵਾਂ ਪੱਟੀ ਨਤੀਜਿਆਂ 'ਚ ਖੋਜ ਸੁਝਾਅ ਵੇਖਾਓ">
<!ENTITY showURLBarSuggestions2.accesskey       "l">
<!ENTITY urlBarSuggestionsPermanentPB.label    "ਟਿਕਾਣਾ ਖੋਜ ਨਤੀਜਿਆਂ ਵਿੱਚ ਖੋਜ ਸੁਝਾਅ ਨਹੀਂ ਵੇਖਾਏ ਜਾਣਗੇ, ਕਿਉਂਕਿ ਤੁਸੀਂ &brandShortName; ਨੂੰ ਕਦੇ ਵੀ ਅਤੀਤ ਯਾਦ ਨਾ ਰੱਖਣ ਲਈ ਸੰਰਚਿਤ ਕੀਤਾ ਹੈ।">

<!ENTITY oneClickSearchEngines.label           "ਇੱਕ-ਕਲਿੱਕ ਖੋਜ ਇੰਜਣ">

<!ENTITY chooseWhichOneToDisplay2.label         "ਬਦਲਵੇਂ ਖੋਜ ਇੰਜਣਾਂ ਨੂੰ ਚੁਣੋ, ਜੋ ਕਿ ਸਿਰਨਾਵਾਂ ਪੱਟੀ ਅਤੇ ਖੋਜ ਪੱਟੀ 'ਚ ਦਿਖਾਈ ਦਿੰਦੇ ਹਨ, ਜਦੋਂ ਕਿ ਤੁਸੀਂ ਕੋਈ ਸ਼ਬਦ ਲਿਖਦੇ ਹੋ।">

<!ENTITY engineNameColumn.label                "ਖੋਜ ਇੰਜਣ">
<!ENTITY engineKeywordColumn.label             "ਸ਼ਬਦ">

<!ENTITY restoreDefaultSearchEngines.label     "ਮੂਲ ਖੋਜ ਇੰਜਣ ਨੂੰ ਮੁੜ-ਸਟੋਰ ਕਰੋ">
<!ENTITY restoreDefaultSearchEngines.accesskey "d">

<!ENTITY removeEngine.label                    "ਹਟਾਓ">
<!ENTITY removeEngine.accesskey                "r">

<!ENTITY findMoreSearchEngines.label           "ਹੋਰ ਖੋਜ ਇੰਜਣ ਲੱਭੋ">
