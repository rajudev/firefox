<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->
<!-- rights.locale-direction instead of the usual local.dir entity, so RTL can skip translating page. -->
<!ENTITY rights.locale-direction "ltr">
<!ENTITY rights.title            "Chi rij Ach'ojib'al">
<!ENTITY rights.intro-header     "Chi rij Ach'ojib'al">
<!ENTITY rights.intro "&brandFullName; jun jamäl kema' chuqa' jaqäl chi nimaläj b'i'aj, ri nuk'un kuma molaj taq winäq aj chijun ri ruwach'ulew. Ja re' ri jujun taq na'oj ri k'atzinel nawetamaj:">

<!-- Note on pointa / pointb / pointc form:
     These points each have an embedded link in the HTML, so each point is
     split into chunks for text before the link, the link text, and the text
     after the link. If a localized grammar doesn't need the before or after
     chunk, it can be left blank.

     Also note the leading/trailing whitespace in strings here, which is
     deliberate for formatting around the embedded links. -->
<!ENTITY rights.intro-point1a "&brandShortName; wachel rik'in re taq rojqanem ri ">
<!ENTITY rights.intro-point1b "Ruwinaqilal ya'öl q'ij Mozilla">
<!ENTITY rights.intro-point1c ". Re re' nuq'ijuj chi yatikïr nawokisaj, nawachib'ej chuqa' nakomonij &brandShortName; kik'in juley chik.  Chuqa' yatikïr najäl ri ruxe'el rub'itz'ib' &brandShortName; achi'el nawajo' richin nuk'äm ri' rik'in ri nawajo'. Ri Aj Winäq Ya'öl Ruq'ij Mozilla nuya' pan ach'ojib'al richin ye'ataluj ri jalon taq ruwäch.">

<!ENTITY rights.intro-point2a "Man yeruya' ta taq ch'ojib'äl ri Mozilla pa kiwi' ri etz'ib'an retal taq rub'i' chuqa' ri ruwachib'al taq rub'i' Mozilla chuqa' Thunderbird. Nawïl rutz'aqat etamab'äl chi kij ri Etz'ib'an Retal taq B'i'aj ">
<!ENTITY rights.intro-point2b "wawe'">
<!ENTITY rights.intro-point2c ".">

<!-- point 3 text for official branded builds -->
<!ENTITY rights.intro-point3a "Ri taq runa'ojil ichinanem kichin ri &vendorShortName; taq tiko'n ye'ilitäj ">
<!ENTITY rights.intro-point3b "wawe'">
<!ENTITY rights.intro-point3c ".">

<!-- point 3 text for unbranded builds -->
<!ENTITY rights.intro-point3-unbranded "Xa b'a achike kina'ojil taq ichinanem okel pa re k'ayij re', rajowaxik niq'alajisäx wawe'.">

<!-- point 4 text for official branded builds -->
<!ENTITY rights.intro-point4a "&brandShortName; chuqa' yerusüj cha'el taq samaj, achi'el ri kik'exik taq tz'aqat; xa xe chi man yojtikïr ta niqajikib'a' chi majun kisachoj o e 100&#37; jikïl. Nawïl ch'aqa' chik rutzijol, ke ri' chuqa' ri rub'eyal richin ye'achüp re taq samaj re', pa taq ">
<!ENTITY rights.intro-point4b "kojqanem samaj">
<!ENTITY rights.intro-point4c ".">

<!-- point 4 text for unbranded builds -->
<!ENTITY rights.intro-point4a-unbranded "We re jun k'ayij re' k'o taq rusamaj k'amaya'l chupam, xa b'a achike chi rojqanem samaj okel pa (taq) samaj k'o ta chi kiximon ki' pa ">
<!ENTITY rights.intro-point4b-unbranded "Kisamaj Ajk'amaya'l taq Ruxaq">
<!ENTITY rights.intro-point4c-unbranded " peraj.">

<!ENTITY rights.webservices-header "&brandFullName; Ajk'amaya'l Ruxaq K'amaya'l taq Samaj">

<!-- Note that this paragraph references a couple of entities from
     preferences/security.dtd, so that we can refer to text the user sees in
     the UI, without this page being forgotten every time those strings are
     updated.  -->
<!-- intro paragraph for branded builds -->
<!ENTITY rights.webservices-a "&brandFullName; yerusüj cha'el taq samaj (&quot; Taq Samaj&quot;), achi'el ri samaj richin kik'exik taq tz'aqat e okel pa re kach'akul ruwäch &brandShortName; achi'el nichol ikim. We man nawajo' ta ye'awokisaj ri taq Samaj o man niqa' ta chawäch ri taq ojqanem ikim, yatikïr nachüp ri samaj richin kik'exik taq tz'aqat rik'in najäq ri taq rajowaxik chokoy, akuchi' nacha' ri ">
<!ENTITY rights.webservices-b "Q'axinäq">
<!ENTITY rights.webservices-c " peraj, ojqan ruma ri ">
<!ENTITY rights.webservices-d "Tik'ex">
<!ENTITY rights.webservices-e " ruwi', chuqa' Kemecha' ri taq rucha'oj &quot;kenik'öx taq k'exoj pa ruyonil richin&quot;.">

<!-- intro paragraph for unbranded builds -->
<!ENTITY rights.webservices-unbranded "Wawe' k'o chi nitz'aqatisäx apo jun ruch'utitzijol kisamaj ajk'amaya'l ruxaq, ri ruk'wan ri k'ayij, chuqa' k'o pe ri taq rucholajil rub'eyal nitz'apïx, we ke ri' rajowaxik.">

<!-- point 1 text for unbranded builds -->
<!ENTITY rights.webservices-term1-unbranded "Konojel ri kojqanem samaj e okel chi re, re k'ayij re', k'atzinel chi e cholotäl qa wawe'.">

<!-- points 1-7 text for branded builds -->
<!ENTITY rights.webservices-term1 "&vendorShortName; nitikïr nuqupij o nuk'ëx ri Samaj ri nawajo' rat.">
<!ENTITY rights.webservices-term2 "Yatikïr ye'awokisaj re taq samaj re' rik'in ri ruwäch &brandShortName;, chuqa' k'o ach'ojib'al richin nab'än.  &vendorShortName; chuqa' ri yeya'on q'ij k'o pa kiq'a' konojel ri ch'aqa' chik taq ch'ojib'äl pa kiwi' ri taq samaj.  Re taq ojqanem re, man nrajo' ta yeruq'ät ri taq ch'ojib'al ya'on ruma jun ya'öl ruq'ij jaqäl b'itz'ib' okisan chi rij ri &brandShortName; chuqa' ri taq ruwäch ejikib'an chi rij ri rub'itz'ib' &brandShortName;.">
<!ENTITY rights.webservices-term3 "Ri taq Samaj esujun &quot;achi'el-re&quot;  &vendorShortName;, ri taq ruto'onela', ya'öl q'ij chuqa' talunela' nikiq'ät jalajöj ruwäch chi retal chilab'enïk, ri b'ixel o man b'ixel ta, chuqa' man nitz'ilöx ta ri b'i'in chik pe, ri retal chilab'enïk chi k'ayel ri samaj chuqa' nuto' ri rurayib'al.  Nak'ül ri k'ayew toq nacha' re samaj re' richin ri arayib'al ke ri' achi'el ri rutzil chuqa' ri rub'eyal nisamäj. Jujun ruchuq'ab'al taq na'oj man nikiya' ta q'ij chi re ri elenem o ri kiq'atik retal chilab'enïk emeb'i'in, ruma ri' re elem rejqalem re' rik'in jub'a' man okel ta pa ri k'ulwachinïk.">
<!ENTITY rights.webservices-term4 "Man ke ta ri' toq taqen tzij chuwäch ri taqanem tzij, &vendorShortName;, ri ruto'onela', ya'öl q'ij chuqa' talunela' man k'o ta kejqalem ruma jun itzelal o roqin, nïm, k'ayewal, k'ulwachel, poqön o tz'etel itzel b'anob'äl, ri ruximon ri' rik'in rokisaxik ri &brandShortName; chuqa' ri taq Samaj.  Ri ajmolaj ejqalem kik'in re taq ojqanem re' k'o chi man nik'o ta chi re ri $500 (Juq'o' wok'al q'anpwäq). Jujun taq ruchuq'ab'al na'oj man nikiya' ta q'ij chi ke'elsäx o keq'at jujun taq itzelal, ruma ri' toq rik'in jub'a' re relesaxik o q'atoj re man okel ta pa ri k'ulwachinel.">
<!ENTITY rights.webservices-term5 "&vendorShortName; ütz ye'ak'ëx re taq ojqanem re we rajowaxik chi ramajil chi ramajil. Man ütz ta yejal o yeq'at we man nuya' ta q'ij pa tz'ib'anïk ri &vendorShortName;.">
<!ENTITY rights.webservices-term6 "Re taq ojqanem re' kiniman ri taqanem rutzij ri California amaq' richin U.S.A., Nel pa rub'ey ri taq chanalinïk pa kiwi' kik'ayewal taqanem taq tzij. We nina'ojïx chi jun peraj chi ke re taq ojqanem re' man e okel ta o man tikirel ta ye'okisäx, ri ch'aqa' chik taq sujunela' xtik'oje' kuchuq'ab'al chuqa' kina'el. K'a te' k'o k'ayewal ri taq ojqanem chi kikojol ri ruwäch tzalq'omin kik'in re taq ruwäch pa q'anchi', xtojqäx ri k'o pa q'anchi'.">
