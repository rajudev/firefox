<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sync">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Connecta\&apos;t al &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Per activar el dispositiu nou, seleccioneu «Configura el &syncBrand.shortName.label;» al dispositiu.'>
<!ENTITY sync.subtitle.pair.label 'Per activar-lo, seleccioneu «Vincula un dispositiu» a l\&apos;altre dispositiu.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'No tinc el dispositiu aquí…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Inicis de sessió'>
<!ENTITY sync.configure.engines.title.history 'Historial'>
<!ENTITY sync.configure.engines.title.tabs 'Pestanyes'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; en &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Menú de les adreces d\&apos;interès'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Etiquetes'>
<!ENTITY bookmarks.folder.toolbar.label 'Barra de les adreces d\&apos;interès'>
<!ENTITY bookmarks.folder.other.label 'Altres adreces d\&apos;interès'>
<!ENTITY bookmarks.folder.desktop.label 'Adreces de l\&apos;escriptori'>
<!ENTITY bookmarks.folder.mobile.label 'Adreces del mòbil'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Fixes'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Reprèn la navegació'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Us donem la benvinguda al &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Inicieu la sessió per sincronitzar les pestanyes, adreces d\&apos;interès, dades d\&apos;inici de sessió i més.'>
<!ENTITY fxaccount_getting_started_get_started 'Inici'>
<!ENTITY fxaccount_getting_started_old_firefox 'Utilitzeu una versió antiga del &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Servidor de comptes'>
<!ENTITY fxaccount_status_sync_now 'Sincronitza ara'>
<!ENTITY fxaccount_status_syncing2 'S\&apos;està sincronitzant…'>
<!ENTITY fxaccount_status_device_name 'Nom del dispositiu'>
<!ENTITY fxaccount_status_sync_server 'Servidor del Sync'>
<!ENTITY fxaccount_status_needs_verification2 'Cal que verifiqueu el compte. Toqueu per reenviar el missatge de verificació.'>
<!ENTITY fxaccount_status_needs_credentials 'No es pot connectar. Toqueu per iniciar la sessió.'>
<!ENTITY fxaccount_status_needs_upgrade 'Heu d\&apos;actualitzar el &brandShortName; per iniciar la sessió.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled 'El &syncBrand.shortName.label; està configurat, però la sincronització automàtica està desactivada. Aneu al menú «Ús de dades» de la configuració de l\&apos;Android i marqueu «Sincronitza dades automàticament».'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 'El &syncBrand.shortName.label; està configurat, però la sincronització automàtica està desactivada. Aneu al menú «Comptes» de la configuració de l\&apos;Android i marqueu «Sincronitza dades automàticament».'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Toqueu per iniciar la sessió al vostre nou compte del Firefox.'>
<!ENTITY fxaccount_status_choose_what 'Trieu què voleu sincronitzar'>
<!ENTITY fxaccount_status_bookmarks 'Adreces d\&apos;interès'>
<!ENTITY fxaccount_status_history 'Historial'>
<!ENTITY fxaccount_status_passwords2 'Inicis de sessió'>
<!ENTITY fxaccount_status_tabs 'Pestanyes obertes'>
<!ENTITY fxaccount_status_additional_settings 'Paràmetres addicionals'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Sincronitza només per Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Fes que el &brandShortName; no se sincronitzi mitjançant les xarxes mòbils o d\&apos;ús limitat'>
<!ENTITY fxaccount_status_legal 'Avisos legals' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Condicions del servei'>
<!ENTITY fxaccount_status_linkprivacy2 'Avís de privadesa'>
<!ENTITY fxaccount_remove_account 'Desconnecta&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Voleu desconnectar-vos del Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Les dades de navegació romandran en aquest dispositiu, però ja no se sincronitzaran amb el vostre compte.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'S\&apos;ha desconnectat el compte del Firefox &formatS;.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Desconnecta'>

<!ENTITY fxaccount_enable_debug_mode 'Activa el mode de depuració'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title 'Opcions del &syncBrand.shortName.label;'>
<!ENTITY fxaccount_options_configure_title 'Configura el &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 'El &syncBrand.shortName.label; no està connectat'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Toqueu per iniciar la sessió com a &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Voleu acabar l\&apos;actualització del &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Toqueu per iniciar la sessió com a &formatS;'>
