<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox ہمہ وار کریں">
<!ENTITY syncBrand.shortName.label "ہمہ وار کریں">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label '&syncBrand.shortName.label; سے جڑیں'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'اپنا نیا آلہ متحرک کرنے کے لیے، آلے پر “Set up &syncBrand.shortName.label;” منتخب کریں۔'>
<!ENTITY sync.subtitle.pair.label 'عمل میں لانے کے لیے، اپنے دوسرے آلے پر “Pair a device” منتخب کریں۔'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'میرے پاس اس وقت آلہ نہیں ہے…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'لاگ ان'>
<!ENTITY sync.configure.engines.title.history 'سابقات'>
<!ENTITY sync.configure.engines.title.tabs 'ٹیب'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS2; پر &formatS1;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'نشانیاں مینیو'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'ٹیگ'>
<!ENTITY bookmarks.folder.toolbar.label 'نشانیاں ٹول بار'>
<!ENTITY bookmarks.folder.other.label 'دیگر نشانیاں'>
<!ENTITY bookmarks.folder.desktop.label 'ڈیسک ٹاپ نشانیاں'>
<!ENTITY bookmarks.folder.mobile.label 'متحرک نشانیاں'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'پن شدہ'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'واپس براؤزنگ پر'>

<!ENTITY fxaccount_getting_started_welcome_to_sync '&syncBrand.shortName.label; میں خوش آمدید'>
<!ENTITY fxaccount_getting_started_description2 'اپنے ٹیب، نشانیاں، پاس ورڈ اور مذید اشیا ہمہ وقت ساز کرنے کے لیے سائن ان کریں۔'>
<!ENTITY fxaccount_getting_started_get_started 'شروع کریں'>
<!ENTITY fxaccount_getting_started_old_firefox '&syncBrand.shortName.label; کا ایک پرانا ورژن استعمال کر رہے ہیں؟'>

<!ENTITY fxaccount_status_auth_server 'اکاؤنٹ پیش کار'>
<!ENTITY fxaccount_status_sync_now 'ابھی ہمہ وار کریں'>
<!ENTITY fxaccount_status_syncing2 'ہمو وار کر رہا ہے…'>
<!ENTITY fxaccount_status_device_name 'آلہ نام'>
<!ENTITY fxaccount_status_sync_server 'ہمہ وار پیش کار'>
<!ENTITY fxaccount_status_needs_verification2 'آپ کے اکاؤنٹ کو توثیق کی ضرورت ہے۔ توثیق ای میل پھر بھیجنے کے لیے کلک کریں۔'>
<!ENTITY fxaccount_status_needs_credentials 'جر نہیں سکتا۔ سائن ان کرنے کے لیے کلک کریں۔'>
<!ENTITY fxaccount_status_needs_upgrade 'سائن ان کرنے کے لیے آپ کو &brandShortName; کا درجہ افزوں کرنا ہو گا۔'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; سیٹ کردیا گیا ہے، لیکن خود بخود ہمہ وار نہیں کر رہا ہے۔ “خود-ہمہ وار کوائف” کو انڈروایڈ &gt; کوائف استعمال کی سیٹنگیں میں ٹوگل کریں۔'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; سیٹ کردیا گیا ہے، لیکن خود بخود ہمہ وار نہیں کر رہا ہے۔ “خود-ہمہ وار کوائف” کو مینیو کے انڈروایڈ &gt; اکاؤنٹس کی سیٹنگیں میں ٹوگل کریں۔'>
<!ENTITY fxaccount_status_needs_finish_migrating 'اپنے نئے Firefox اکاؤنٹ میں سائن ان کرنے کے لیے کلک کریں۔'>
<!ENTITY fxaccount_status_choose_what 'چنیں کیا ہمہ وقت ساز کرنا ہے'>
<!ENTITY fxaccount_status_bookmarks 'نشانیاں'>
<!ENTITY fxaccount_status_history 'سابقات'>
<!ENTITY fxaccount_status_passwords2 'لاگ ان'>
<!ENTITY fxaccount_status_tabs 'ٹیب کھولیے'>
<!ENTITY fxaccount_status_additional_settings 'اضافی سیٹنگیں'>
<!ENTITY fxaccount_pref_sync_use_metered2 'صرف بعظریہ وائی فائی ہمہ وقت سازی کریں'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_status_legal 'قانونی' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'خدمت کی کی شرائط'>
<!ENTITY fxaccount_status_linkprivacy2 'اطلاع نامہ نجی نوعیت'>
<!ENTITY fxaccount_remove_account 'منقطع کریں&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'ھمہ وقت سازی سے رابطہ منقطع کریں؟'>
<!ENTITY fxaccount_remove_account_dialog_message2 'آپ کے براؤزنگ کے کوائف اس آلہ پر رہیں گے، لیکن یہ آپ کے اکاؤنٹ کے ساتھ ہمہ وقت ساز نہیں رہیں گے۔'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Firefox اکاؤنٹ &formatS; ،نقظع کر دیا گیا۔'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'منقطع کریں'>

<!ENTITY fxaccount_enable_debug_mode 'ڈیبگ موڈ کو اہل بنائیں'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; اختیارات'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label; تشکیل کریں'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; جڑا ہوا نہیں ہے'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 '&formatS; سے سائن ان کرنے کے لیے کلک کریں'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title '&syncBrand.shortName.label; کی فزوں کاری مکمل؟'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text '&formatS; کے طور پر سائن ان کرنے کے لیے کلک کریں'>
