<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Note: each tab panel must contain unique accesskeys -->

<!ENTITY generalTab.label                "ಸಾಮಾನ್ಯ">

<!ENTITY useCursorNavigation.label       "ಪುಟದಲ್ಲಿ ಸಂಚರಿಸಲು ಎಲ್ಲಾ ಸಮಯದಲ್ಲೂ ತೆರೆಸೂಚಕ ಕೀಲಿಗಳನ್ನು ಬಳಸು">
<!ENTITY useCursorNavigation.accesskey   "c">
<!ENTITY searchOnStartTyping.accesskey   "x">
<!ENTITY useOnScreenKeyboard.label       "ಅಗತ್ಯಬಿದ್ದಾಗ ಒಂದು ಟಚ್ ಕೀಲಮಣೆಯನ್ನು ತೋರಿಸು">
<!ENTITY useOnScreenKeyboard.accesskey   "k">

<!ENTITY browsing.label                  "ವೀಕ್ಷಣೆ">

<!ENTITY useAutoScroll.label             "ಸ್ವಯಂಚಲನೆಯನ್ನು(ಆಟೋ ಸ್ಕ್ರಾಲಿಂಗ್) ಬಳಸು">
<!ENTITY useAutoScroll.accesskey         "a">
<!ENTITY useSmoothScrolling.label        "ಮೃದು ಚಲನೆಯನ್ನು ಬಳಸು">
<!ENTITY useSmoothScrolling.accesskey    "m">
<!ENTITY checkUserSpelling.accesskey     "t">

<!ENTITY dataChoicesTab.label            "ದತ್ತಾಂಶ ಆಯ್ಕೆಗಳು">

<!-- LOCALIZATION NOTE (healthReportingDisabled.label): This message is displayed above
disabled data sharing options in developer builds or builds with no Telemetry support
available. -->

<!ENTITY enableHealthReport2.accesskey   "r">
<!ENTITY healthReportLearnMore.label     "ಇನ್ನಷ್ಟು ತಿಳಿಯಿರಿ">

<!ENTITY dataCollection.label            "&brandShortName; ದತ್ತಾಂಶ ಸಂಗ್ರಹಣೆ ಮತ್ತು ಬಳಕೆ">
<!ENTITY dataCollectionPrivacyNotice.label    "ಗೌಪ್ಯತಾ ಸೂಚನೆ">

<!ENTITY collectBrowserErrors.accesskey      "b">
<!ENTITY collectBrowserErrorsLearnMore.label "ಇನ್ನಷ್ಟು ತಿಳಿಯಿರಿ">

<!ENTITY alwaysSubmitCrashReports1.accesskey "c">
<!ENTITY crashReporterLearnMore.label    "ಇನ್ನಷ್ಟು ತಿಳಿಯಿರಿ">

<!ENTITY networkTab.label                "ಜಾಲಬಂಧ">

<!ENTITY networkProxy.label              "ಜಾಲಬಂಧ ಪ್ರಾಕ್ಸಿ">

<!ENTITY connectionDesc.label            "&brandShortName; ಹೇಗೆ ಅಂತರಜಾಲಕ್ಕೆ ಸಂಪರ್ಕಿತಗೊಳ್ಳಬೇಕೆಂದು ಸಂರಚಿಸಿ ">

<!ENTITY connectionSettingsLearnMore.label "ಇನ್ನಷ್ಟು ತಿಳಿಯಿರಿ">
<!ENTITY connectionSettings.label        "ಸಿದ್ಧತೆಗಳು...">
<!ENTITY connectionSettings.accesskey    "e">

<!ENTITY httpCache.label                 "ಕ್ಯಾಶ್ ಮಾಡಲಾದ ಜಾಲದ ವಿಷಯಗಳು">

<!--  Site Data section manages sites using Storage API and is under Network -->
<!ENTITY siteData.label                  "ಜಾಲತಾಣ ದತ್ತಾಂಶ">
<!ENTITY clearSiteData.label             "ದತ್ತಾಂಶ ಬರಿದುಮಾಡು">
<!ENTITY clearSiteData.accesskey         "l">
<!ENTITY siteDataSettings.label          "ಸಿದ್ಧತೆಗಳು...">
<!ENTITY siteDataSettings.accesskey      "i">
<!ENTITY siteDataLearnMoreLink.label     "ಇನ್ನಷ್ಟು ತಿಳಿಯಿರಿ">

<!-- LOCALIZATION NOTE:
  The entities limitCacheSizeBefore.label and limitCacheSizeAfter.label appear on a single
  line in preferences as follows:

  &limitCacheSizeBefore.label [textbox for cache size in MB] &limitCacheSizeAfter.label;
-->
<!ENTITY limitCacheSizeBefore.label      "ಕ್ಯಾಶೆಯನ್ನು ಇದಕ್ಕೆ ಮಿತಿಗೊಳಿಸು">
<!ENTITY limitCacheSizeBefore.accesskey  "L">
<!ENTITY limitCacheSizeAfter.label       "MB ಯಷ್ಟು ಸ್ಥಳ">
<!ENTITY clearCacheNow.label             "ಈಗಲೆ ತೆರವುಗೊಳಿಸು">
<!ENTITY clearCacheNow.accesskey         "C">
<!ENTITY overrideSmartCacheSize.label    "ಸ್ವಯಂಚಾಲಿತ ಕ್ಯಾಶೆ ನಿರ್ವಹಣೆಯನ್ನು ಅತಿಕ್ರಮಿಸು">
<!ENTITY overrideSmartCacheSize.accesskey "O">

<!ENTITY updateTab.label                 "ಅಪ್ಡೇಟ್‍">

<!-- LOCALIZATION NOTE (updateApplication.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApplication.label         "&brandShortName; ನವೀಕರಣಗಳು">
<!-- LOCALIZATION NOTE (updateApplication.version.*): updateApplication.version.pre
# is followed by a version number, keep the trailing space or replace it with a
# different character as needed. updateApplication.version.post is displayed
# after the version number, and is empty on purpose for English. You can use it
# if required by your language.
 -->
<!ENTITY updateApplication.version.pre   "ಆವೃತ್ತಿ">
<!ENTITY updateApplication.version.post  "">
<!ENTITY updateApplication.description   "&brandShortName; ಅನುಮತಿಸು">
<!ENTITY updateAuto3.accesskey           "A">
<!ENTITY updateCheckChoose2.label        "ಅಪ್‌ಡೇಟ್‌ಗಳಿಗಾಗಿ ಹುಡುಕುತ್ತದೆ, ಆದರೆ ಅವುಗಳನ್ನು ಅನುಸ್ಥಾಪಿಸುವ ಆಯ್ಕೆಯನ್ನು ನಿಮಗೆ ಬಿಡುತ್ತದೆ">
<!ENTITY updateCheckChoose2.accesskey    "C">
<!ENTITY updateManual2.accesskey         "N">

<!ENTITY updateHistory2.label            "ಅಪ್ಡೇಟ್ ಇತಿಹಾಸವನ್ನು ತೋರಿಸು…">
<!ENTITY updateHistory2.accesskey        "p">

<!ENTITY useService.label                "ಅಪ್‌ಡೇಟ್‌ಗಳನ್ನು ಅನುಸ್ಥಾಪಿಸಲು ಹಿನ್ನಲೆ ಸೇವೆಯನ್ನು ಬಳಸು">
<!ENTITY useService.accesskey            "b">

<!ENTITY enableSearchUpdate2.accesskey   "e">

<!ENTITY certificateTab.label            "ಪ್ರಮಾಣಪತ್ರಗಳು">
<!ENTITY certPersonal2.description       "ಒಂದು ಪರಿಚಾರಕವು ನನ್ನ ಖಾಸಗಿ ಪ್ರಮಾಣಪತ್ರವನ್ನು ಅಪೇಕ್ಷಿಸಿದಾಗ">
<!ENTITY selectCerts.auto                "ಸ್ವಯಂಚಾಲಿತವಾಗಿ ಒಂದನ್ನು ಆರಿಸು">
<!ENTITY selectCerts.auto.accesskey      "S‍">
<!ENTITY selectCerts.ask                 "ಪ್ರತಿ ಬಾರಿಯೂ ನನ್ನನ್ನು ಕೇಳು">
<!ENTITY selectCerts.ask.accesskey       "A‍">
<!ENTITY enableOCSP.label                "ಪ್ರಮಾಣಪತ್ರಗಳ ಪ್ರಸಕ್ತ ಮಾನ್ಯತೆಯನ್ನು ಖಚಿತಪಡಿಸಿಕೊಳ್ಳಲು OCSP ರೆಸ್ಪಾಂಡರ್ ಪೂರೈಕೆಗಣಕಗಳಿಗೆ ಮನವಿ ಮಾಡಿ">
<!ENTITY enableOCSP.accesskey            "Q">
<!ENTITY viewCerts2.label                "ಪ್ರಮಾಣಪತ್ರಗಳನ್ನು ನೋಡು…">
<!ENTITY viewCerts2.accesskey            "C">
<!ENTITY viewSecurityDevices2.label      "ಸುರಕ್ಷತಾ ಸಾಧನಗಳು…">
<!ENTITY viewSecurityDevices2.accesskey  "D">

<!ENTITY performance.label               "ಕಾರ್ಯಕ್ಷಮತೆ">
<!ENTITY useRecommendedPerformanceSettings2.accesskey
                                         "U">
<!ENTITY performanceSettingsLearnMore.label
                                         "ಇನ್ನಷ್ಟು ತಿಳಿಯಿರಿ">
<!ENTITY limitContentProcessOption.label "ಪರಿವಿಡಿ ಪ್ರಕ್ರೀಯೆ ಮಟ್ಟ">
<!ENTITY limitContentProcessOption.accesskey   "L">
<!ENTITY allowHWAccel.label              "ಲಭ್ಯವಿದ್ದಾಗ ಯಂತ್ರಾಂಶ ವೇಗವರ್ಧನೆಯನ್ನು ಬಳಸು">
<!ENTITY allowHWAccel.accesskey          "r">
