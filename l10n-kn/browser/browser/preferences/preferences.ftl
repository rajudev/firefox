# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

do-not-track-description = ನೀವು ಟ್ರ್ಯಾಕ್ ಮಾಡಲು ಬಯಸದ ಜಾಲತಾಣಗಳಿಗೆ "ಡು ನಾಟ್ ಟ್ರ್ಯಾಕ್" ಸಿಗ್ನಲ್ ಅನ್ನು ಕಳುಹಿಸಿ
do-not-track-learn-more = ಇನ್ನಷ್ಟು ತಿಳಿಯಿರಿ
do-not-track-option-default =
    .label = ಜಾಡು ಇರಿಸುವಿಕೆ ರಕ್ಷಣೆಯನ್ನು ಬಳಸುವಾಗ ಮಾತ್ರ
do-not-track-option-always =
    .label = ಯಾವಾಗಲೂ
pref-page =
    .title = { PLATFORM() ->
            [windows] ಆಯ್ಕೆಗಳು
           *[other] ಆದ್ಯತೆಗಳು
        }
# This is used to determine the width of the search field in about:preferences,
# in order to make the entire placeholder string visible
#
# Notice: The value of the `.style` attribute is a CSS string, and the `width`
# is the name of the CSS property. It is intended only to adjust the element's width.
# Do not translate.
search-input =
    .style = width: 15.4em
pane-general-title = ಸಾಮಾನ್ಯ
category-general =
    .tooltiptext = { pane-general-title }
pane-search-title = ಹುಡುಕು
category-search =
    .tooltiptext = { pane-search-title }
pane-privacy-title = ಗೋಪ್ಯತೆ & ಸುರಕ್ಷತೆ
category-privacy =
    .tooltiptext = { pane-privacy-title }
# The word "account" can be translated, do not translate or transliterate "Firefox".
pane-sync-title = Firefox ಖಾತೆ
category-sync =
    .tooltiptext = { pane-sync-title }
help-button-label = { -brand-short-name } ಸಹಾಯ
focus-search =
    .key = f
close-button =
    .aria-label = ಮುಚ್ಚು

## Browser Restart Dialog

feature-enable-requires-restart = ಈ ಸೌಲಭ್ಯವನ್ನು ಸಕ್ರಿಯಗೊಳಿಸಲು { -brand-short-name } ಅನ್ನು ಮರಳಿ ಆರಂಭಿಸಬೇಕು.
feature-disable-requires-restart = ಈ ಸೌಲಭ್ಯವನ್ನು ನಿಷ್ಕ್ರಿಯಗೊಳಿಸಲು { -brand-short-name } ಅನ್ನು ಮರಳಿ ಆರಂಭಿಸಬೇಕು.
should-restart-title = { -brand-short-name } ಅನ್ನು ಮರು ಆರಂಭಿಸು
should-restart-ok = ಈಗ { -brand-short-name } ಮರಳಿ ಆರಂಭಿಸು
revert-no-restart-button = ಹಿಂದಿನ ಸ್ಥಿತಿಗೆ ಮರಳಿಸು
restart-later = ಆಮೇಲೆ ಮರು ಆರಂಭಿಸು
