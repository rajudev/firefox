# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = Táto stránka zobrazuje údaje o výkonnosti a využívaní funkcií zozbierané pomocou telemetrie. Informácie sú anonymne odosielané spoločnosti %1$S s cieľom vylepšiť program %2$S.

# LOCALIZATION NOTE(homeExplanation):
# - %1$S is either telemetryEnabled or telemetryDisabled
# - %2$S is either extendedTelemetryEnabled or extendedTelemetryDisabled
homeExplanation = Telemetria je %1$S a rozšírená telemetria je %2$S.
telemetryEnabled = povolená
telemetryDisabled = zakázaná
extendedTelemetryEnabled = povolená
extendedTelemetryDisabled = zakázaná

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
settingsExplanation = Telemetria zbiera %1$S a odosielanie je %2$S.
releaseData = informácie o vydaní
prereleaseData = informácie pred vydaním
telemetryUploadEnabled = povolené
telemetryUploadDisabled = zakázané

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
pingDetails = Všetky informácie sú odosielané ako súčasť “%1$S”. Teraz sa pozeráte na ping %2$S.
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = %1$S, %2$S
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
pingDetailsCurrent = Všetky informácie sú odosielané ako súčasť “%1$S“. Teraz sa pozeráte na ping %2$S.
pingExplanationLink = pings
currentPing = súčasný

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = Hľadať v sekcii %1$S
filterAllPlaceholder = Hľadať vo všetkých sekciách

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = Výsledky vyhľadávania pre „%1$S“
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = Mrzí nás to, no pre hľadaný výraz „%2$S“ sme v sekcii %1$S nič nenašli
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
noSearchResultsAll = Mrzí nás to, no pre hľadaný výraz „%S“ sme v žiadnej sekcii nič nenašli
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
noDataToDisplay = Mrzí nás to, no v sekcii „%S“ nie sú dostupné žiadne údaje
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
currentPingSidebar = súčasný ping
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = všetko

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = vzoriek
histogramAverage = priemer
histogramSum = súčet
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = Kopírovať

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = Záznamy telemetrie
telemetryLogHeadingId = Identifikátor
telemetryLogHeadingTimestamp = Časová známka
telemetryLogHeadingData = Údaje

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = Pomalé výrazy SQL v hlavnom vlákne
slowSqlOther = Pomalé výrazy SQL v pomocných vláknach
slowSqlHits = Počet
slowSqlAverage = Priem. čas (ms)
slowSqlStatement = Výraz

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = Identifikátor doplnku
addonTableDetails = Podrobnosti
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = %1$S Provider

keysHeader = Kľúč
namesHeader = Názov
valuesHeader = Hodnota

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = Správa o zlyhaní #%1$S (%2$S sekúnd)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
captured-stacks-title = %1$S (počet zachytení: %2$S)
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = Neskorý zápis #%1$S

stackTitle = Zásobník:
memoryMapTitle = Mapa pamäte:

errorFetchingSymbols = Pri získavaní symbolov sa vyskytla chyba. Uistite sa, že počítač je pripojený k sieti Internet a skúste to znova.

parentPayload = Nadradený obsah
# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
childPayloadN = Obsah potomka %1$S
timestampHeader = časová známka
categoryHeader = kategória
methodHeader = metóda
objectHeader = objekt
extraHeader = extra
