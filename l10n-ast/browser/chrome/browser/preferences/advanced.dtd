<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Note: each tab panel must contain unique accesskeys -->

<!ENTITY generalTab.label                "Xeneral">

<!ENTITY useCursorNavigation.label       "Usar siempre les tecles del cursor pa restolar dientro de les páxines">
<!ENTITY useCursorNavigation.accesskey   "c">
<!ENTITY searchOnStartTyping.label       "Guetar el testu mientres s'escribe">
<!ENTITY searchOnStartTyping.accesskey   "s">
<!ENTITY useOnScreenKeyboard.label       "Amosar un tecláu en pantalla cuando seya necesario">
<!ENTITY useOnScreenKeyboard.accesskey   "t">

<!ENTITY browsing.label                  "Restolar">

<!ENTITY useAutoScroll.label             "Usar desplazamientu automáticu">
<!ENTITY useAutoScroll.accesskey         "d">
<!ENTITY useSmoothScrolling.label        "Usar desplazamientu sele">
<!ENTITY useSmoothScrolling.accesskey    "l">
<!ENTITY checkUserSpelling.label         "Comprobar ortografía mientres s'escribe">

<!ENTITY dataChoicesTab.label            "Eleición de datos">

<!-- LOCALIZATION NOTE (healthReportingDisabled.label): This message is displayed above
disabled data sharing options in developer builds or builds with no Telemetry support
available. -->

<!ENTITY healthReportLearnMore.label     "Deprender más">

<!ENTITY dataCollectionPrivacyNotice.label    "Avisu de privacidá">

<!ENTITY crashReporterLearnMore.label    "Deprender más">

<!ENTITY networkTab.label                "Rede">

<!ENTITY networkProxy.label              "Proxy de rede">

<!ENTITY connectionDesc.label            "Configurar cómo se coneuta &brandShortName; a Internet">
<!ENTITY connectionSettings.label        "Configuración…">
<!ENTITY connectionSettings.accesskey    "o">

<!ENTITY httpCache.label                 "Conteníu web na caché">

<!ENTITY offlineStorage2.label           "Conteníu web y datos d'usuariu en mou ensin conexón">

<!--  Site Data section manages sites using Storage API and is under Network -->
<!ENTITY siteData.label                  "Datos del sitiu">
<!ENTITY clearSiteData.label             "Llimpiar tolos datos">
<!ENTITY siteDataSettings.label          "Axustes…">
<!ENTITY siteDataSettings.accesskey      "x">
<!ENTITY siteDataLearnMoreLink.label     "Deprendi más">

<!-- LOCALIZATION NOTE:
  The entities limitCacheSizeBefore.label and limitCacheSizeAfter.label appear on a single
  line in preferences as follows:

  &limitCacheSizeBefore.label [textbox for cache size in MB] &limitCacheSizeAfter.label;
-->
<!ENTITY limitCacheSizeBefore.label      "Llendar caché a">
<!ENTITY limitCacheSizeBefore.accesskey  "L">
<!ENTITY limitCacheSizeAfter.label       "MB d'espaciu">
<!ENTITY clearCacheNow.label             "Llimpiar agora">
<!ENTITY clearCacheNow.accesskey         "g">
<!ENTITY clearOfflineAppCacheNow.label   "Llimpiar agora">
<!ENTITY clearOfflineAppCacheNow.accesskey "r">
<!ENTITY overrideSmartCacheSize.label    "Inorar l'alministración automática de caché">
<!ENTITY overrideSmartCacheSize.accesskey "n">

<!ENTITY updateTab.label                 "Anovar">

<!-- LOCALIZATION NOTE (updateApplication.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApplication.label         "Anovamientos de &brandShortName;">
<!-- LOCALIZATION NOTE (updateApplication.version.*): updateApplication.version.pre
# is followed by a version number, keep the trailing space or replace it with a
# different character as needed. updateApplication.version.post is displayed
# after the version number, and is empty on purpose for English. You can use it
# if required by your language.
 -->
<!ENTITY updateApplicationDescription.label
                                         "Caltén &brandShortName; anováu pa un meyor rindimientu, estabilidá y seguranza.">
<!ENTITY updateApplication.version.pre   "Versión ">
<!ENTITY updateApplication.version.post  "">
<!ENTITY updateApplication.description   "Permitir a &brandShortName;:">
<!ENTITY updateAuto3.label               "Instalar anovamientos automáticamente (recomendáu)">
<!ENTITY updateCheckChoose2.label        "Guetar anovamientos pero dexar escoyer cuándo instalalos">
<!ENTITY updateManual2.label             "Nun guetar por anovamientos (nun se recomienda)">

<!ENTITY updateHistory2.label            "Amosar l'historial d'anovamientos…">
<!ENTITY updateHistory2.accesskey        "t">

<!ENTITY useService.label                "Usar serviciu en segundu planu pa instalar los anovamientos">
<!ENTITY useService.accesskey            "v">

<!ENTITY enableSearchUpdate2.label       "Anovar automáticamente motores de gueta">

<!ENTITY offlineStorageNotify.label               "Avisame cuando s'entrugue pa guardar datos en mou desconexón">
<!ENTITY offlineStorageNotify.accesskey           "T">
<!ENTITY offlineStorageNotifyExceptions.label     "Esceiciones…">
<!ENTITY offlineStorageNotifyExceptions.accesskey "s">

<!ENTITY offlineAppsList3.label          "Los siguientes sitios tienen permisu pa guardar datos pal mou ensin conexón">
<!ENTITY offlineAppsList.height          "7em">
<!ENTITY offlineAppsListRemove.label     "Desaniciar...">
<!ENTITY offlineAppsListRemove.accesskey "i">
<!ENTITY offlineAppRemove.confirm        "Desaniciar conteníu en mou desconeutáu">

<!ENTITY certificateTab.label            "Certificaos">
<!ENTITY certPersonal2.description       "Cuando un sirvidor solicite'l to certificáu personal:">
<!ENTITY selectCerts.auto                "Seleicionar ún automáticamente">
<!ENTITY selectCerts.auto.accesskey      "S">
<!ENTITY selectCerts.ask                 "Entrugame cada vegada">
<!ENTITY selectCerts.ask.accesskey       "A">
<!ENTITY enableOCSP.label                "Consultar a los sirvidores respondedores OCSP pa confirmar la validez actual de los certificaos">
<!ENTITY enableOCSP.accesskey            "u">
<!ENTITY viewCerts2.label                "Ver certificaos…">
<!ENTITY viewCerts2.accesskey            "C">
<!ENTITY viewSecurityDevices2.label      "Preseos de seguridá…">
<!ENTITY viewSecurityDevices2.accesskey  "D">

<!ENTITY performance.label               "Rindimientu">
<!ENTITY useRecommendedPerformanceSettings2.label
                                         "Usar axustes recomendaos de rindimientu">
<!ENTITY useRecommendedPerformanceSettings2.description
                                         "Estos axustes adáutense al hardware y sistema operativu del to equipu.">
<!ENTITY useRecommendedPerformanceSettings2.accesskey
                                         "U">
<!ENTITY performanceSettingsLearnMore.label
                                         "Deprendi más">
<!ENTITY allowHWAccel.label              "Usar aceleración de hardware cuando tea disponible">
<!ENTITY allowHWAccel.accesskey          "h">
