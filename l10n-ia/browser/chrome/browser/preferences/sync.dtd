<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- The page shown when logged in... -->

<!ENTITY engine.bookmarks.label     "Marcapaginas">
<!ENTITY engine.bookmarks.accesskey "m">
<!ENTITY engine.tabs.label2         "Schedas aperite">
<!ENTITY engine.tabs.title          "Un lista de lo que es aperte sur tote le apparatos synchronisate">
<!ENTITY engine.tabs.accesskey      "T">
<!ENTITY engine.history.label       "Chronologia">
<!ENTITY engine.history.accesskey   "r">
<!ENTITY engine.logins.label        "Authenticationes">
<!ENTITY engine.logins.title        "Nomines de usator e contrasignos que tu salvava">
<!ENTITY engine.logins.accesskey    "A">
<!-- On Windows we use the term "Options" to describe settings, but
     on Linux and Mac OS X we use "Preferences" - carry that distinction
     over into this string, used as the checkbox which indicates if prefs are synced
-->
<!ENTITY engine.prefsWin.label      "Optiones">
<!ENTITY engine.prefsWin.accesskey  "S">
<!ENTITY engine.prefs.label         "Preferentias">
<!ENTITY engine.prefs.accesskey     "S">
<!ENTITY engine.prefs.title         "Parametros gederal, de confidentialitate e de securitate que tu modificava">
<!ENTITY engine.addons.label        "Additivos">
<!ENTITY engine.addons.title        "Extensiones e themas pro Firefox scriptorio">
<!ENTITY engine.addons.accesskey    "A">
<!ENTITY engine.addresses.label     "Adresses">
<!ENTITY engine.addresses.title     "Adresses postal que tu salvava (solo scriptorio)">
<!ENTITY engine.addresses.accesskey "e">
<!ENTITY engine.creditcards.label   "Cartas de credito">
<!ENTITY engine.creditcards.title   "Nomines, numeros e datas de expiration (solo scriptorio)">
<!ENTITY engine.creditcards.accesskey "C">

<!-- Device Settings -->
<!ENTITY fxaSyncDeviceName.label       "Nomine del apparato">
<!ENTITY changeSyncDeviceName2.label "Cambiar le nomine del apparato…">
<!ENTITY changeSyncDeviceName2.accesskey "C">
<!ENTITY cancelChangeSyncDeviceName.label "Cancellar">
<!ENTITY cancelChangeSyncDeviceName.accesskey "n">
<!ENTITY saveChangeSyncDeviceName.label "Salvar">
<!ENTITY saveChangeSyncDeviceName.accesskey "v">

<!-- Footer stuff -->
<!ENTITY prefs.tosLink.label        "Terminos de servicio">
<!ENTITY fxaPrivacyNotice.link.label "Advertentia de confidentialitate">

<!-- LOCALIZATION NOTE (signedInUnverified.beforename.label,
signedInUnverified.aftername.label): these two string are used respectively
before and after the account email address. Localizers can use one of them, or
both, to better adapt this sentence to their language.
-->
<!ENTITY signedInUnverified.beforename.label "">
<!ENTITY signedInUnverified.aftername.label "non es verificate.">

<!-- LOCALIZATION NOTE (signedInLoginFailure.beforename.label,
signedInLoginFailure.aftername.label): these two string are used respectively
before and after the account email address. Localizers can use one of them, or
both, to better adapt this sentence to their language.
-->
<!ENTITY signedInLoginFailure.beforename.label "Per favor authentica te pro te reconnecter">
<!ENTITY signedInLoginFailure.aftername.label "">

<!ENTITY notSignedIn.label            "Tu non es connectite.">
<!ENTITY signIn.label                 "Authenticar se">
<!ENTITY signIn.accesskey             "A">
<!ENTITY profilePicture.tooltip       "Cambiar le imagine del profilo">
<!ENTITY verifiedManage.label         "Gerer le conto">
<!ENTITY verifiedManage.accesskey     "o">
<!ENTITY disconnect3.label            "Disconnecter se…">
<!ENTITY disconnect3.accesskey        "D">
<!ENTITY verify.label                "Verificar le email">
<!ENTITY verify.accesskey            "V">
<!ENTITY forget.label                "Oblidar iste email">
<!ENTITY forget.accesskey            "O">

<!ENTITY resendVerification.label     "Reinviar le verification">
<!ENTITY resendVerification.accesskey "d">
<!ENTITY cancelSetup.label            "Cancellar le configuration">
<!ENTITY cancelSetup.accesskey        "p">

<!ENTITY signedOut.caption            "Porta le web con te">
<!ENTITY signedOut.description        "Synchronisa tu marcapaginas, chronologia, contrasignos, additivos, e preferentias inter tote tu apparatos.">
<!ENTITY signedOut.accountBox.title   "Connecter se con un &syncBrand.fxAccount.label;">
<!ENTITY signedOut.accountBox.create2 "Non ha tu un conto? Que nos comencia">
<!ENTITY signedOut.accountBox.create2.accesskey "C">
<!ENTITY signedOut.accountBox.signin2 "Authenticar se…">
<!ENTITY signedOut.accountBox.signin2.accesskey "I">

<!ENTITY signedIn.settings.label       "Parametros de Sync">
<!ENTITY signedIn.settings.description "Elige lo que tu vole synchronisar in tu apparatos usante &brandShortName;.">

<!-- LOCALIZATION NOTE (mobilePromo3.*): the following strings will be used to
     create a single sentence with active links.
     The resulting sentence in English is: "Download Firefox for
     Android or iOS to sync with your mobile device." -->

<!ENTITY mobilePromo3.start            "Discarga Firefox pro ">
<!-- LOCALIZATION NOTE (mobilePromo3.androidLink): This is a link title that links to https://www.mozilla.org/firefox/android/ -->
<!ENTITY mobilePromo3.androidLink      "Android">

<!-- LOCALIZATION NOTE (mobilePromo3.iOSBefore): This is text displayed between mobilePromo3.androidLink and mobilePromo3.iosLink -->
<!ENTITY mobilePromo3.iOSBefore         " o ">
<!-- LOCALIZATION NOTE (mobilePromo3.iOSLink): This is a link title that links to https://www.mozilla.org/firefox/ios/ -->
<!ENTITY mobilePromo3.iOSLink          "iOS">

<!ENTITY mobilePromo3.end              " pro synchronisar te con tu apparato mobile.">

<!ENTITY mobilepromo.singledevice      "Connecte altere apparato">
<!ENTITY mobilepromo.multidevice       "Gerer le apparatos">
