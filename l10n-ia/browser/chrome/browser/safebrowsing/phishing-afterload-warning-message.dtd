<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Regressar">
<!ENTITY safeb.palm.seedetails.label "Vider le detalios">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Iste sito non es fraudulente…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Aviso providite per <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Visitar iste sito web pote nocer a tu computator">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; blocava iste pagina perque illo pote tentar de installar malware que pote robar o deler informationes personal sur tu computator.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> esseva <a id='error_desc_link'>reportate continer malware</a>. Tu pote <a id='report_detection'>reportar un problema de detection</a> o <a id='ignore_warning_link'>ignorar le risco</a> e ir a iste sito non secur.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> esseva <a id='error_desc_link'>reportate continer malware</a>. Tu pote <a id='report_detection'>reportar un problema de detection</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Apprende plus re contento de web periculose como viruses e altere malware e re como proteger tu computator in <a id='learn_more_link'>StopBadware.org</a>. Apprende plus re &brandShortName;’s Protection ab le phishing e le malware a <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Iste sito pote continer programmas nocive">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; blocava iste pagina perque illo pote tentar de dupar te a installar programmas que noce a tu experientia de navigation (per exemplo, modificante tu pagina principal o monstrante annuncios extra sur le sitos que tu visita).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> ha essite <a id='error_desc_link'>reportate como continente software nocive</a>. Tu pote <a id='ignore_warning_link'>ignorar le risco</a> e ir a iste sito non secur.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> ha essite <a id='error_desc_link'>reportate como continente software nocive</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Apprende altero re le software malefic e indesiderate a <a id='learn_more_link'>Regulas del software indesiderate</a>. Apprende altero re &brandShortName;’s protection de phishing e malware a <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Sito sequente fraudulente">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; blocava iste pagina perque illo pote dupar te a facer alco de damnose como installar software o revelar informationes personal como contrasignos o cartas de credito.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> ha essite <a id='error_desc_link'>reportate como un sito periculose</a>. Tu pote <a id='report_detection'>reportar un problema de revelation</a> o <a id='ignore_warning_link'>ignorar le risco</a> e ir a iste sito insecur.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> ha essite <a id='error_desc_link'>reportate como un sito periculose</a>. Tu pote <a id='report_detection'>reportar un problema de revelation</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Apprende de plus re le sitos periculose a <a id='learn_more_link'>www.antiphishing.org</a>. Apprende de plus re &brandShortName;’s Protection del phishing e del malware a <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Iste sito pote continer malware">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; ha blocate iste pagina perque illo poterea tentar installar applicationes periculose que roba o dele tu informationes (pro exemplo, photos, contrasignos, messages e cartas de credito).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> ha essite <a id='error_desc_link'>reportate como continente un application potentialmente periculose</a>. Tu pote <a id='ignore_warning_link'>ignorar le risco</a> e ir a iste sito insecur.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> ha essite <a id='error_desc_link'>reportate como continente un application potentialmente periculose</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Apprende de plus re &brandShortName;’s Protection del phishing e del malware a <a id='firefox_support'>support.mozilla.org</a>.">
