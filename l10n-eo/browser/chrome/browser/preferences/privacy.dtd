<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  trackingProtectionHeader2.label      "Protekto kontraŭ spurado">
<!ENTITY  trackingProtection2.description      "Spurado estas la kolekto de viaj retumaj datumoj tra pluraj retejoj. Spurado povas esti uzita por konstrui profilon kaj montri enhavon surbaze de viaj retumaj kaj personaj informoj.">
<!ENTITY  trackingProtection2.radioGroupLabel  "Uzi protekton kontraŭ spurado por bloki konatajn spurilojn">
<!ENTITY  trackingProtection3.description      "La protekto kontraŭ spurado blokas retajn spurilojn, kiuj kolektas viajn retumajn datumojn tra pluraj retejoj.">
<!ENTITY  trackingProtection3.radioGroupLabel  "Uzi protekton kontraŭ spurado por bloki konatajn spurilojn">
<!ENTITY  trackingProtectionAlways.label       "Ĉiam">
<!ENTITY  trackingProtectionAlways.accesskey   "m">
<!ENTITY  trackingProtectionPrivate.label      "Nur en privataj fenestroj">
<!ENTITY  trackingProtectionPrivate.accesskey  "p">
<!ENTITY  trackingProtectionNever.label        "Neniam">
<!ENTITY  trackingProtectionNever.accesskey    "N">
<!ENTITY  trackingProtectionLearnMore.label    "Pli da informo">
<!ENTITY  trackingProtectionLearnMore2.label    "Pli da informo pri protekto kontraŭ spurado kaj via privateco">
<!ENTITY  trackingProtectionExceptions.label   "Esceptoj…">
<!ENTITY  trackingProtectionExceptions.accesskey "E">

<!-- LOCALIZATION NOTE (trackingProtectionPBM6.label): This string is displayed if privacy.trackingprotection.ui.enabled is set to false. This currently happens on the release and beta channel. -->
<!ENTITY trackingProtectionPBM6.label         "Uzi protekton kontraŭ spurado dum privata retumo por bloki konatajn spurilojn">
<!ENTITY trackingProtectionPBM6.accesskey     "U">
<!ENTITY trackingProtectionPBMLearnMore.label "Pli da informo">
<!ENTITY changeBlockList2.label               "Ŝanĝi liston de blokado…">
<!ENTITY changeBlockList2.accesskey           "b">

<!ENTITY  doNotTrack.description        "Sendi al retejoj sciigon, ke vi ne volas esti spurata">
<!ENTITY  doNotTrack.learnMore.label    "Pli da informo">
<!ENTITY  doNotTrack.default.label      "Nur dum uzo de protekto kontraŭ spurado">
<!ENTITY  doNotTrack.always.label       "Ĉiam">

<!ENTITY  history.label                 "Historio">
<!ENTITY  permissions.label             "Permesoj">

<!ENTITY  addressBar.label              "Adresa strio">
<!ENTITY  addressBar.suggest.label      "Dum uzo de la retadresa strio, sugesti el">
<!ENTITY  locbar.history2.label         "retuma historio">
<!ENTITY  locbar.history2.accesskey     "h">
<!ENTITY  locbar.bookmarks.label        "legosignoj">
<!ENTITY  locbar.bookmarks.accesskey    "l">
<!ENTITY  locbar.openpage.label         "malfermitaj langetoj">
<!ENTITY  locbar.openpage.accesskey     "m">
<!ENTITY  locbar.searches.label         "Rilatitaj serĉoj el la norma serĉilo">
<!ENTITY  locbar.searches.accesskey     "R">

<!ENTITY  suggestionSettings2.label     "Ŝanĝi preferojn pri serĉilaj sugestoj">

<!ENTITY  acceptCookies2.label          "Akcepti kuketojn el retejoj">
<!ENTITY  acceptCookies2.accesskey      "A">

<!ENTITY  acceptThirdParty2.pre.label     "Akcepti nerektajn kuketojn">
<!ENTITY  acceptThirdParty2.pre.accesskey "n">

<!ENTITY  acceptCookies3.label          "Akcepti kuketojn kaj retejajn datumojn el retejoj">
<!ENTITY  acceptCookies3.accesskey      "A">

<!ENTITY  blockCookies.label            "Bloki kuketojn kaj retejajn datumojn (tio povus misfunkciigi retejojn)">
<!ENTITY  blockCookies.accesskey        "B">

<!ENTITY  acceptThirdParty3.pre.label     "Akcepti nerektajn kuketojn kaj retejajn datumojn">
<!ENTITY  acceptThirdParty3.pre.accesskey "n">
<!ENTITY  acceptThirdParty.always.label   "Ĉiam">
<!ENTITY  acceptThirdParty.never.label    "Neniam">
<!ENTITY  acceptThirdParty.visited.label  "El vizititaj retejoj">

<!ENTITY  keepUntil2.label              "Konservi ĝis">
<!ENTITY  keepUntil2.accesskey          "K">

<!ENTITY  expire.label                  "ili senvalidiĝas">
<!ENTITY  close.label                   "mi finas &brandShortName;">

<!ENTITY  cookieExceptions.label        "Esceptoj…">
<!ENTITY  cookieExceptions.accesskey    "t">

<!ENTITY  showCookies.label             "Montri kuketojn…">
<!ENTITY  showCookies.accesskey         "M">

<!ENTITY  historyHeader2.pre.label         "&brandShortName;">
<!ENTITY  historyHeader2.pre.accesskey     "F">
<!ENTITY  historyHeader.remember.label     "memoros historion">
<!ENTITY  historyHeader.dontremember.label "neniam memoros historion">
<!ENTITY  historyHeader.custom.label       "uzos personajn agordojn por la historio">
<!ENTITY  historyHeader.post.label         "">

<!ENTITY  rememberDescription.label      "&brandShortName; memoros la historion de via retumo, elŝutoj, serĉoj kaj formularoj, kaj ĝi konservos kuketojn de retejoj kiujn vi vizitas.">

<!-- LOCALIZATION NOTE (rememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.middle.label): include a starting and trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.post.label): include a starting space as needed -->
<!ENTITY  rememberActions.pre.label           "Vi eble volos ">
<!ENTITY  rememberActions.clearHistory.label  "forviŝi vian ĵusan historion">
<!ENTITY  rememberActions.middle.label        ", aŭ ">
<!ENTITY  rememberActions.removeCookies.label "forviŝi individuajn kuketojn">
<!ENTITY  rememberActions.post.label          ".">

<!ENTITY  dontrememberDescription.label  "&brandShortName; uzos la samajn agordojn de privata retumo, kaj ĝi ne memoros iun historion dum vi esploras la reton.">

<!-- LOCALIZATION NOTE (dontrememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (dontrememberActions.post.label): include a starting space as needed -->
<!ENTITY  dontrememberActions.pre.label          "Vi eble volos ankaŭ ">
<!ENTITY  dontrememberActions.clearHistory.label "forviŝi la tutan aktualan historion">
<!ENTITY  dontrememberActions.post.label         ".">

<!ENTITY  clearHistoryButton.label       "Viŝi historion…">
<!ENTITY  clearHistoryButton.accesskey   "V">

<!ENTITY  privateBrowsingPermanent2.label "Ĉiam uzi la reĝimon de privata retumo">
<!ENTITY  privateBrowsingPermanent2.accesskey "p">

<!ENTITY  rememberHistory2.label      "Memori mian historion de retumo kaj elŝutoj">
<!ENTITY  rememberHistory2.accesskey  "h">

<!ENTITY  rememberSearchForm.label       "Memori historion de serĉadoj kaj formularoj">
<!ENTITY  rememberSearchForm.accesskey   "s">

<!ENTITY  clearOnClose.label             "Forviŝi historion kiam &brandShortName; finiĝas">
<!ENTITY  clearOnClose.accesskey         "v">

<!ENTITY  clearOnCloseSettings.label     "Agordoj…">
<!ENTITY  clearOnCloseSettings.accesskey "g">

<!ENTITY  browserContainersLearnMore.label      "Pli da informo">
<!ENTITY  browserContainersEnabled.label        "Aktivigi ingajn langetojn">
<!ENTITY  browserContainersEnabled.accesskey    "i">
<!ENTITY  browserContainersSettings.label        "Agordoj…">
<!ENTITY  browserContainersSettings.accesskey    "A">

<!ENTITY  a11yPrivacy.checkbox.label     "Eviti ke alireblecaj servoj aliru vian retumilon">
<!ENTITY  a11yPrivacy.checkbox.accesskey "E">
<!ENTITY  a11yPrivacy.learnmore.label    "Pli da informo">
<!ENTITY enableSafeBrowsingLearnMore.label "Pli da informo">
