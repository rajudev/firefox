<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Gean tebek">
<!ENTITY safeb.palm.seedetails.label "Details besjen">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Dit is gjin misliedende website…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "m">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Advys útjûn troch <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Dizze website kin skeadlik wêze foar jo kompjûter">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; hat dizze side blokkearre, omdat dizze probearje kin kweawollende software te ynstallearjen dy't persoanlike gegevens op jo kompjûter stelle of fuortsmite kin.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> is <a id='error_desc_link'>rapportearre as in website dy't kweawollende software befettet</a>. jo kinne <a id='report_detection'>in deteksjeprobleem rapportearje</a> of <a id='ignore_warning_link'>it risiko negearje</a> en dizze ûnfeilige website besykje.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> is <a id='error_desc_link'>rapportearre as in website dy't kweawollende software befettet</a>. Jo kinne <a id='report_detection'>in deteksjeprobleem rapportearje</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Lâs mear oer skeadlike webynhâd, wêrûnder firussen en oare malware, en it beskermjen fan jo kompjûter op <a id='learn_more_link'>StopBadware.org</a>. Lês mear oer beskerming tsjin phishing en malware fan &brandShortName; op <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "De folgjende website kin skeadlike programma's befetsje">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; hat dizze side blokkearre, omdat dizze probearje kinne soe jo programma’s ynstallearje te litten dy't skea oan jo surfûnderfining tabringe kinne (bygelyks troch jo startside te wizigjen of ekstra advertinsjes te toanen op websites dy't jo besykje).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> is <a id='error_desc_link'>rapportearre as in website dy't skeadlike software befettet</a>. Jo kinne <a id='ignore_warning_link'>it risiko negearje</a> en dizze ûnfeilige website besykje.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> is <a id='error_desc_link'>rapportearre as in website dy't skeadlike software befettet</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Lês mear oer skeadlike en net winske software op <a id='learn_more_link'>Belied ta oansjen fan net winske software</a>. Lês mear oer beskerming tsjin phishing en malware fan &brandShortName; op <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Misliedende website">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; hat dizze side blokkearre, omdat dizze probearje kin jo wat gefaarliks dwaan te litten, lykas it ynstallearjen fan software of it dielen fan persoanlike gegevens lykas wachtwurden of creditcardgegevens.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> is <a id='error_desc_link'>rapportearre as in misliedende website</a>. Jo kinne <a id='report_detection'>in deteksjeprobleem rapportearje</a> of <a id='ignore_warning_link'>it risiko negearje</a> en dizze ûnfeilige website besykje.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> is <a id='error_desc_link'>rapportearre as in misliedende website</a>. Jo kinne <a id='report_detection'>in deteksjeprobleem rapportearje</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Lês mear oer misliedende websites en phishing op <a id='learn_more_link'>www.antiphishing.org</a>. Lês mear oer beskerming tsjin phishing en malware fan &brandShortName; op <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "De folgjende website kin malware befetsje">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; hat dizze side blokkearre, omdat dizze kin probearje gefaarlike apps te ynstallearjen dy't jo gegevens (lykas foto’s, wachtwurden, berjochten en creditcardgegevens) stelle of fuortsmite.">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> is <a id='error_desc_link'>rapportearre as in website dy't in mooglik skeadlike tapassing befettet</a>. Jo kinne <a id='ignore_warning_link'>it risiko negearje</a> en dizze ûnfeilige website besykje.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> is <a id='error_desc_link'>rapportearre as in website dy't in mooglik skeadlike tapassing befettet</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Lês mear oer beskerming tsjin phishing en malware fan &brandShortName; op <a id='firefox_support'>support.mozilla.org</a>.">
