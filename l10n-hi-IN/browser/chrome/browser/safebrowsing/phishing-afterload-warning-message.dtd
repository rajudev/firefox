<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "वापस जाएँ">
<!ENTITY safeb.palm.seedetails.label "विवरण देखें">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "यह एक भ्रमकारी भुजाल नहीं है…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "d">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "<a id='advisory_provider'/> के द्वारा परामर्श प्रदान की गई.">


<!ENTITY safeb.blocked.malwarePage.title2 "इस वेबसाइट पर जाना आपके कंप्यूटर के लिए हानिकारी हो सकता है">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; ने इस पृष्ठ को अवरुद्ध किया है क्योंकि यह द्वेषपूर्ण सॉफ़्टवेयर स्थापित करने का प्रयास कर सकता है जो आपके कंप्यूटर पर व्यक्तिगत जानकारी चुरा या हटा सकता है.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> को <a id='error_desc_link'>द्वेषपूर्ण सॉफ़्टवेयर युक्त बताया गया है</a>. आप <a id='report_detection'>पहचान की समस्या की रिपोर्ट कर सकते हैं</a> या <a id='ignore_warning_link'>जोखिम को अनदेखा कर सकते हैं</a> और इस असुरक्षित साइट पर जा सकते हैं.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> को <a id='error_desc_link'>द्वेषपूर्ण सॉफ़्टवेयर युक्त बताया गया है</a>. आप <a id='report_detection'>पहचान की समस्या की रिपोर्ट कर सकते हैं</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "वायरस और अन्य मैलवेयर सहित हानिकारक वेब सामग्री और अपने कंप्यूटर को सुरक्षित करने के बारे में <a id='learn_more_link'>StopBadware.org</a> पर और जानें. &brandShortName; के फ़िशिंग और मैलवेयर सुरक्षा के बारे में <a id='firefox_support'>support.mozilla.org</a> पर और जानें.">


<!ENTITY safeb.blocked.unwantedPage.title2 "अगली साइट में हानिकारक प्रोग्राम हो सकते हैं">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; ने इस पृष्ठ को अवरुद्ध कर दिया क्योंकि यह आपको ऐसे प्रोग्राम संस्थापित करने में उलझाने का प्रयास कर सकता है जो आपके ब्राउज़िंग अनुभव को नुकसान पहुँचाते हैं (उदाहरण के लिए, अपने मुखपृष्ठ को बदलकर या आपके द्वारा देखी जाने वाली साइटों पर अतिरिक्त विज्ञापन दिखाकर).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> को <a id='error_desc_link'>हानिकारक सॉफ़्टवेयर युक्त सूचित किया गया है</a>. आप <a id='ignore_warning_link'>जोखिम को अनदेखा</a> कर सकते हैं और इस असुरक्षित साइट पर जा सकते हैं.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> को <a id='error_desc_link'>हानिकारक सॉफ़्टवेयर युक्त सूचित किया गया है</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "हानिकारक और अवांछित सॉफ़्टवेयर के बारे में <a id='learn_more_link'>अवांछित सॉफ़्टवेयर नीति</a> पर और जानें. &brandShortName; के फ़िशिंग और मैलवेयर सुरक्षा के बारे में <a id='firefox_support'>support.mozilla.org</a> पर और जानें.">


<!ENTITY safeb.blocked.phishingPage.title3 "आगे धोखादायक साइट">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; ने इस पृष्ठ को अवरुद्ध किया क्योंकि यह आपको सॉफ़्टवेयर अधिस्थापित करने अथवा पासवर्ड या क्रेडिट कार्ड जैसी व्यक्तिगत जानकारी का खुलासा करने जैसी खतरनाक कार्य करने में छल कर सकता हैं.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> को <a id='error_desc_link'>एक भ्रामक साइट के रूप में प्रतिवेदित किया गया है</a>. आप <a id='report_detection'>एक पहचान की समस्या की रिपोर्ट कर सकते हैं</a> या <a id='ignore_warning_link'>जोखिम को अनदेखा करें</a> और इस असुरक्षित साइट पर जाएँ.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> को <a id='error_desc_link'>एक भ्रामक साइट के रूप में प्रतिवेदित किया गया है</a>. आप <a id='report_detection'>एक पहचान की समस्या की रिपोर्ट कर सकते हैं</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "धोखादायक साइटों और फ़िशिंग के बारे में <a id='learn_more_link'>www.antiphishing.org</a> पर और जानें. &brandShortName; के फ़िशिंग और मैलवेयर संरक्षण के बारे में <a id='firefox_support'>support.mozilla.org</a> पर और जानें.">


<!ENTITY safeb.blocked.harmfulPage.title "आगे की साइट में मैलवेयर हो सकता है">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; ने इस पृष्ठ को अवरोधित कर दिया क्योंकि यह ख़तरनाक ऐप्स अधिस्थापित करने का प्रयास कर सकता है जो कि आपकी जानकारियाँ (जैसे फ़ोटो, पासवर्ड, संदेश और क्रेडिट कार्ड) चुरा अथवा मिटा सकती हैं.">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> को <a id='error_desc_link'>संभावित हानिकारक अनुप्रयोग युक्त प्रतिवेदित किया गया है</a>. आप <a id='ignore_warning_link'>जोखिम को अनदेखा कर</a> इस असुरक्षित साइट पर जा सकते हैं.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> को <a id='error_desc_link'>संभावित हानिकारक अनुप्रयोग युक्त प्रतिवेदित किया गया है</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "&brandShortName; के फ़िशिंग और मैलवेयर संरक्षण के बारे में <a id='firefox_support'>support.mozilla.org</a> पर और जानें.">
