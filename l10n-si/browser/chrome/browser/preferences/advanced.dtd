<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Note: each tab panel must contain unique accesskeys -->

<!ENTITY generalTab.label                "සාමාන්‍ය">

<!ENTITY useCursorNavigation.label       "සැම විටම පිටුව තුළ සැරිසැරීමට කර්සර යතුරු භාවිතා කරන්න">
<!ENTITY useCursorNavigation.accesskey   "c">
<!ENTITY searchOnStartTyping.label       "ඔබ යතුරුකරණය ආරම්භ කළ විට පෙළ සඳහා සොයන්න">
<!ENTITY searchOnStartTyping.accesskey   "x">
<!ENTITY useOnScreenKeyboard.label       "අවශ්‍ය විටක ස්පර්ශක යතුරු පුවරුව පෙන්වන්න">
<!ENTITY useOnScreenKeyboard.accesskey   "k">

<!ENTITY browsing.label                  "ගවේෂණය">

<!ENTITY useAutoScroll.label             "ස්වයංක්‍රීයව ස්ක්‍රෝල් වීම භාවිතා කරන්න">
<!ENTITY useAutoScroll.accesskey         "a">
<!ENTITY useSmoothScrolling.label        "සුමට ස්ක්‍රෝල් වීම භාවිතා කරන්න">
<!ENTITY useSmoothScrolling.accesskey    "m">
<!ENTITY checkUserSpelling.label         "ඔබ යතුරු ලියන අතර අක්ෂර වින්‍යාසය පිරික්සන්න">
<!ENTITY checkUserSpelling.accesskey     "t">

<!ENTITY dataChoicesTab.label            "දත්ත තෝරාගැනීම්">

<!-- LOCALIZATION NOTE (healthReportingDisabled.label): This message is displayed above
disabled data sharing options in developer builds or builds with no Telemetry support
available. -->

<!ENTITY healthReportLearnMore.label     "තවත් දැනගන්න">



<!ENTITY crashReporterLearnMore.label    "තවත් දැනගන්න">

<!ENTITY networkTab.label                "ජාලය">


<!ENTITY connectionDesc.label            "&brandShortName; අන්තර්ජාලයට සම්බන්ධ වන අයුරු සැකසීම">

<!ENTITY connectionSettings.label        "සැකසුම්...">
<!ENTITY connectionSettings.accesskey    "e">

<!ENTITY httpCache.label                 "කෑෂ් ගතකළ වෙබ් අන්තර්ගතය">

<!--  Site Data section manages sites using Storage API and is under Network -->
<!ENTITY siteData.label                  "අඩවි දත්ත">
<!ENTITY clearSiteData.label             "සියළු දත්ත හිස් කරන්න">
<!ENTITY clearSiteData.accesskey         "l">
<!ENTITY siteDataSettings.label          "සිටුවම්…">
<!ENTITY siteDataSettings.accesskey      "i">
<!ENTITY siteDataLearnMoreLink.label     "තවත් දැනගන්න">

<!-- LOCALIZATION NOTE:
  The entities limitCacheSizeBefore.label and limitCacheSizeAfter.label appear on a single
  line in preferences as follows:

  &limitCacheSizeBefore.label [textbox for cache size in MB] &limitCacheSizeAfter.label;
-->
<!ENTITY limitCacheSizeBefore.label      "cache මෙයට සීමා කරන්න">
<!ENTITY limitCacheSizeBefore.accesskey  "L">
<!ENTITY limitCacheSizeAfter.label       "MB ක හිස් ඉඩක්">
<!ENTITY clearCacheNow.label             "දැන් හිස් කරන්න">
<!ENTITY clearCacheNow.accesskey         "C">
<!ENTITY overrideSmartCacheSize.label    "ස්වයංක්‍රීය කෑෂ් කළමනාකරණය ඉක්මවා යන්න (Override)">
<!ENTITY overrideSmartCacheSize.accesskey "O">

<!ENTITY updateTab.label                 "යාවත්කාලීන">

<!-- LOCALIZATION NOTE (updateApplication.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApplication.label         "&brandShortName; යාවත්කාල">
<!-- LOCALIZATION NOTE (updateApplication.version.*): updateApplication.version.pre
# is followed by a version number, keep the trailing space or replace it with a
# different character as needed. updateApplication.version.post is displayed
# after the version number, and is empty on purpose for English. You can use it
# if required by your language.
 -->
<!ENTITY updateAuto3.label               "ස්වයංක්‍රීයව යාවත් ස්ථාපනය කරන්න (නිර්දේශිත)">
<!ENTITY updateAuto3.accesskey           "A">
<!ENTITY updateCheckChoose2.label        "යාවත් සඳහා සොයන්න නමුත් ස්ථාපනය සඳහා ඔබෙන් විමසන්න">
<!ENTITY updateCheckChoose2.accesskey    "C">
<!ENTITY updateManual2.label             "කිසිවිට යාවත් සඳහා නොවිමසන්න (නිර්දේශිත නොවේ)">
<!ENTITY updateManual2.accesskey         "N">

<!ENTITY updateHistory2.label            "යාවත් ඉතිහාසය පෙන්වන්න…">
<!ENTITY updateHistory2.accesskey        "p">

<!ENTITY useService.label                "යාවත්කාලීන ස්ථාපනය සඳහා පසුබ්ම් සේවාව (background service) භාවිතා කරන්න">
<!ENTITY useService.accesskey            "b">

<!ENTITY enableSearchUpdate2.label       "සෙවුම් යන්ත්‍ර ස්වයංක්‍රීයව යාවත් කරන්න">
<!ENTITY enableSearchUpdate2.accesskey   "e">

<!ENTITY certificateTab.label            "සහතික">
<!ENTITY certPersonal2.description       "සේවාදායකයක් වෙතින් ඔබේ පුද්ගලික සහතික ඉල්ලාසිටින විට">
<!ENTITY selectCerts.auto                "එකක් ස්වයංක්‍රීයව තෝරන්න">
<!ENTITY selectCerts.auto.accesskey      "S">
<!ENTITY selectCerts.ask                 "සැමවිටම ඔබෙන් විමසන්න">
<!ENTITY selectCerts.ask.accesskey       "A">
<!ENTITY enableOCSP.label                "සහතිකයන්හි වත්මන් වලංගුභාවය තහවුරු කිරීම සඳහා OCSP ප්‍රතිචාර සේවාදායක විමසන්න">
<!ENTITY enableOCSP.accesskey            "Q">
<!ENTITY viewCerts2.label                "සහතික පෙන්වන්න…">
<!ENTITY viewCerts2.accesskey            "C">
<!ENTITY viewSecurityDevices2.label      "ආරක්ෂක උපාංග…">
<!ENTITY viewSecurityDevices2.accesskey  "D">

<!ENTITY performance.label               "ක්‍රියාකාරීත්වය">
<!ENTITY useRecommendedPerformanceSettings2.label
                                         "නිර්දේශිත ක්‍රියාකාරීත්ව සැකසුම් භාවිත කරන්න">
<!ENTITY useRecommendedPerformanceSettings2.description
                                         "මෙම සැකසුම් ඔබේ පරිගණකයේ දෘඩාංග සහ මෙහෙයුම් පද්ධතිය සඳහා සුදුසු ලෙස සැකසී ඇත.">
<!ENTITY useRecommendedPerformanceSettings2.accesskey
                                         "U">
<!ENTITY performanceSettingsLearnMore.label
                                         "තවත් දැනගන්න">
<!ENTITY limitContentProcessOption.label "අන්තර්ගත සැකසුම් සීමාව">
<!ENTITY limitContentProcessOption.description
                                         "">
<!ENTITY allowHWAccel.label              "ඇත්නම් දෘඩාංග වේග-උපාංග (acceleration) භාවිතා කරන්න">
<!ENTITY allowHWAccel.accesskey          "r">
