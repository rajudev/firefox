<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY safeb.palm.accept.label2 "Gå tillbaka">
<!ENTITY safeb.palm.seedetails.label "Se detaljer">

<!-- Localization note (safeb.palm.notdeceptive.label) - Label of the Help menu
  item. Either this or reportDeceptiveSiteMenu.label from report-phishing.dtd is
  shown. -->
<!ENTITY safeb.palm.notdeceptive.label "Detta är inte en vilseledande webbplats…">
<!-- Localization note (safeb.palm.notdeceptive.accesskey) - Because
  safeb.palm.notdeceptive.label and reportDeceptiveSiteMenu.title from
  report-phishing.dtd are never shown at the same time, the same accesskey can
  be used for them. -->
<!ENTITY safeb.palm.notdeceptive.accesskey "v">

<!-- Localization note (safeb.palm.advisory.desc) - Please don't translate <a id="advisory_provider"/> tag.  It will be replaced at runtime with advisory link-->
<!ENTITY safeb.palm.advisory.desc2 "Rådgivning tillhandahålls av <a id='advisory_provider'/>.">


<!ENTITY safeb.blocked.malwarePage.title2 "Att besöka denna webbplats kan skada din dator">
<!ENTITY safeb.blocked.malwarePage.shortDesc2 "&brandShortName; blockerade den här sidan eftersom den kan försöka installera skadlig programvara som kan stjäla eller radera personlig information på din dator.">

<!-- Localization note (safeb.blocked.malwarePage.errorDesc.override, safeb.blocked.malwarePage.errorDesc.noOverride, safeb.blocked.malwarePage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.malwarePage.errorDesc.override "<span id='malware_sitename'/> har <a id='error_desc_link'>rapporterats innehålla skadlig programvara</a>. Du kan <a id='report_detection'>rapportera ett detekteringsproblem</a> eller <a id='ignore_warning_link'>ignorera risken</a> och fortsätta till denna osäkra webbplats.">

<!ENTITY safeb.blocked.malwarePage.errorDesc.noOverride "<span id='malware_sitename'/> har <a id='error_desc_link'>rapporterats innehålla skadlig programvara</a>. Du kan <a id='report_detection'>rapportera ett detekteringsproblem</a>.">

<!ENTITY safeb.blocked.malwarePage.learnMore "Läs mer om skadligt webbinnehåll inklusive virus och annan skadlig kod och hur du skyddar din dator på <a id='learn_more_link'>StopBadware.org</a>. Läs mer om skydd mot nätfiske och skadlig programvara i &brandShortName; på <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.unwantedPage.title2 "Kommande webbplats kan innehålla skadliga program">
<!ENTITY safeb.blocked.unwantedPage.shortDesc2 "&brandShortName; blockerade den här sidan eftersom den kan lura dig att installera program som skadar din webbupplevelse (till exempel genom att ändra din startsida eller visa extra annonser på webbplatser du besöker).">

<!-- Localization note (safeb.blocked.unwantedPage.errorDesc.override, safeb.blocked.unwantedPage.errorDesc.noOverride, safeb.blocked.unwantedPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->

<!ENTITY safeb.blocked.unwantedPage.errorDesc.override "<span id='unwanted_sitename'/> har <a id='error_desc_link'>rapporterats innehålla skadlig programvara</a>. Du kan <a id='ignore_warning_link'>ignorera risken</a> och gå till den här osäkra webbplatsen.">

<!ENTITY safeb.blocked.unwantedPage.errorDesc.noOverride "<span id='unwanted_sitename'/> har <a id='error_desc_link'>rapporterats innehålla skadlig programvara</a>.">

<!ENTITY safeb.blocked.unwantedPage.learnMore "Läs mer om skadlig och oönskad programvara på <a id='learn_more_link'>Unwanted Software Policy</a>. Läs mer om skydd mot nätfiske och skadlig programvara i &brandShortName; på <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.phishingPage.title3 "Vilseledande webbplats">
<!ENTITY safeb.blocked.phishingPage.shortDesc3 "&brandShortName; blockerade den här sidan eftersom den försöker lura dig att göra något farligt som att installera programvara eller avslöja personlig information som lösenord eller kreditkort.">

<!-- Localization note (safeb.blocked.phishingPage.errorDesc.override, safeb.blocked.phishingPage.errorDesc.noOverride, safeb.blocked.phishingPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.phishingPage.errorDesc.override "<span id='phishing_sitename'/> har <a id='error_desc_link'>rapporterats som en vilseledande webbplats</a>. Du kan <a id='report_detection'>rapportera ett detektionsproblem</a> eller <a id='ignore_warning_link'>ignorera risken</a> och gå till denna osäkra webbplatsen.">

<!ENTITY safeb.blocked.phishingPage.errorDesc.noOverride "<span id='phishing_sitename'/> har <a id='error_desc_link'>rapporterats som en vilseledande webbplats</a>. Du kan <a id='report_detection'>rapportera ett detekteringsproblem</a>.">

<!ENTITY safeb.blocked.phishingPage.learnMore "Läs mer om vilseledande webbplatser och nätfiske på <a id='learn_more_link'>www.antiphishing.org</a>. Läs mer om skydd mot nätfiske och skadlig programvara i &brandShortName; på <a id='firefox_support'>support.mozilla.org</a>.">


<!ENTITY safeb.blocked.harmfulPage.title "Kommande webbplats kan innehålla skadlig kod">
<!ENTITY safeb.blocked.harmfulPage.shortDesc2 "&brandShortName; blockerade den här sidan eftersom det kan försöka installera farliga appar som stjäl eller raderar din information (till exempel bilder, lösenord, meddelanden och kreditkort).">

<!-- Localization note (safeb.blocked.harmfulPage.errorDesc.override, safeb.blocked.harmfulPage.errorDesc.noOverride, safeb.blocked.harmfulPage.learnMore) - All <span> and <a> tags are replaced by the appropriate links and text during runtime. -->
<!ENTITY safeb.blocked.harmfulPage.errorDesc.override "<span id='harmful_sitename'/> har <a id='error_desc_link'>rapporterats innehålla ett potentiellt skadligt program</a>. Du kan <a id='ignore_warning_link'>ignorera risken</a> och gå till denna osäkra webbplatsen.">

<!ENTITY safeb.blocked.harmfulPage.errorDesc.noOverride "<span id='harmful_sitename'/> har <a id='error_desc_link'>rapporterats innehålla ett potentiellt skadligt program</a>.">

<!ENTITY safeb.blocked.harmfulPage.learnMore "Läs mer om skydd mot nätfiske och skadlig programvara i &brandShortName; på <a id='firefox_support'>support.mozilla.org</a>.">
