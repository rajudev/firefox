<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Sync">
<!ENTITY syncBrand.shortName.label "Sanngoɗin">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Seŋo e &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Ngam hurminde kaɓirgol maa kesol ngol, labo “Set up &syncBrand.shortName.label;” e kaɓirgol ngol.'>
<!ENTITY sync.subtitle.pair.label 'Ngam hurminde, labo “Jokkondir Kaɓirɗi” e kaɓirgol maa ngola.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Mi jogaaki kaɓirgol ngol jooni…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Ceŋorɗe'>
<!ENTITY sync.configure.engines.title.history 'Aslol'>
<!ENTITY sync.configure.engines.title.tabs 'Tabbe'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; e &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Dosol Maantore'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Tage'>
<!ENTITY bookmarks.folder.toolbar.label 'Palal Maantore'>
<!ENTITY bookmarks.folder.other.label 'Maantore Goɗɗe'>
<!ENTITY bookmarks.folder.desktop.label 'Maantore Biro'>
<!ENTITY bookmarks.folder.mobile.label 'Maantore Cinndel'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Ñippaaɗe'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Rutto to banngagol'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'A jaɓɓaama e &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Seŋo ngam syncude tabbe maa, maantore e ceŋorɗe &amp; geɗe goɗɗe.'>
<!ENTITY fxaccount_getting_started_get_started 'Fuɗɗo ɗoo'>
<!ENTITY fxaccount_getting_started_old_firefox 'Njogi-ɗaa ko yamre hiiɗnde &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Sarwrorde konte'>
<!ENTITY fxaccount_status_sync_now 'Sanngoɗin jooni'>
<!ENTITY fxaccount_status_syncing2 'Nana synca…'>
<!ENTITY fxaccount_status_device_name 'Innde kaɓirgol'>
<!ENTITY fxaccount_status_sync_server 'Sarworde sync'>
<!ENTITY fxaccount_status_needs_verification2 'Konte maa ena cokli ƴeewteede. Tappu ngam neltirde iimeel ƴeewtindol.'>
<!ENTITY fxaccount_status_needs_credentials 'Horiima seŋaade. Tappu ngam seŋaade.'>
<!ENTITY fxaccount_status_needs_upgrade 'Aɗa sokli hasɗitinde &brandShortName; ngam seŋaade.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; teeltaama, kono wonaa e syncude e jaajol. Toggilo “Auto-sync data” e Teelte Android &gt; Data Usage.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; teeltaama, kono wonaani e syncude e jaajol. Toggilo “Auto-sync data” e dosol Teelte Android &gt; Konte.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Tappu ngam seŋaade e Konte Firefox maa kese'>
<!ENTITY fxaccount_status_choose_what 'Suɓo ko njahdintaa'>
<!ENTITY fxaccount_status_bookmarks 'Maantore'>
<!ENTITY fxaccount_status_history 'Aslol'>
<!ENTITY fxaccount_status_passwords2 'Ceŋorɗe'>
<!ENTITY fxaccount_status_tabs 'Uddit tabbe'>
<!ENTITY fxaccount_status_additional_settings 'Teelte Goɗɗe'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Yahdin e WI-FI tan'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Haɗ &brandShortName; yahdinde e laylaytol selluleer walla ɓetangol'>
<!ENTITY fxaccount_status_legal 'Laawol' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Sarɗiiji carwol'>
<!ENTITY fxaccount_status_linkprivacy2 'Tintinol suturo'>
<!ENTITY fxaccount_remove_account 'Seŋto&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Seŋto e Jahdingol?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Keɓe banngagol maa keddoyto e ngel masiŋel, kono ɗum nattat yahdinde e konte maa.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Konte Firefox &formatS; seŋtiima.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Seŋto'>

<!ENTITY fxaccount_enable_debug_mode 'Hurmin Mbayka buggitagol'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; Cuɓe'>
<!ENTITY fxaccount_options_configure_title 'Teelto &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; seŋaaki'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Tappu ngam seŋaade &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Timmin kesɗitinal &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Tappu ngam seŋaade e &formatS;'>
