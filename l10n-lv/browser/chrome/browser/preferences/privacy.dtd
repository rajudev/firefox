<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  trackingProtectionHeader2.label      "Izsekošanas aizsardzība">
<!ENTITY  trackingProtection2.description      "Izsekotāji vāc datus par jums dažādās lapās. Šo informāciju var izmantot, lai veidotu jūsu profilu un attēlotu pielāgotu saturu un reklāmas.">
<!ENTITY  trackingProtection2.radioGroupLabel  "Izmantot izsekošanas aizsardzību, lai bloķētu zināmos izsekotājus">
<!ENTITY  trackingProtection3.description      "Izsekošanas aizsardzība cīnās pret sekotājiem kas dažādās lapās vāc informāciju par jūsu uzvedību internetā.">
<!ENTITY  trackingProtection3.radioGroupLabel  "Izmantot izsekošanas aizsardzību, lai bloķētu zināmos izsekotājus">
<!ENTITY  trackingProtectionAlways.label       "Vienmēr">
<!ENTITY  trackingProtectionAlways.accesskey   "m">
<!ENTITY  trackingProtectionPrivate.label      "Tikai privātajos logos">
<!ENTITY  trackingProtectionPrivate.accesskey  "l">
<!ENTITY  trackingProtectionNever.label        "Nekad">
<!ENTITY  trackingProtectionNever.accesskey    "N">
<!ENTITY  trackingProtectionLearnMore.label    "Uzzināt vairāk">
<!ENTITY  trackingProtectionLearnMore2.label    "Uzziniet vairāk par izsekošanas aizsardzību un jūsu privātumu">
<!ENTITY  trackingProtectionExceptions.label   "Izņēmumi…">
<!ENTITY  trackingProtectionExceptions.accesskey "I">

<!-- LOCALIZATION NOTE (trackingProtectionPBM6.label): This string is displayed if privacy.trackingprotection.ui.enabled is set to false. This currently happens on the release and beta channel. -->
<!ENTITY trackingProtectionPBM6.label         "Izmantot izsekošanas aizsardzību, lai bloķētu zināmos izsekotājus">
<!ENTITY trackingProtectionPBM6.accesskey     "t">
<!ENTITY trackingProtectionPBMLearnMore.label "Uzzināt vairāk">
<!ENTITY changeBlockList2.label               "Mainīt bloķēto sarakstu…">
<!ENTITY changeBlockList2.accesskey           "M">

<!ENTITY  doNotTrack.description        "Sūtīt lapām “Do Not Track” signālu, lai norādītu, ka nevēlaties, lai jūs izseko">
<!ENTITY  doNotTrack.learnMore.label    "Uzzināt vairāk">
<!ENTITY  doNotTrack.default.label      "Tagad izmantojot izsekošanas aizsardzību">
<!ENTITY  doNotTrack.always.label       "Vienmēr">

<!ENTITY  history.label                 "Vēsture">
<!ENTITY  permissions.label             "Atļaujas">

<!ENTITY  addressBar.label              "Adrešu josla">
<!ENTITY  addressBar.suggest.label      "Meklējot adreses joslā, ieteikt">
<!ENTITY  locbar.history2.label         "Pārlūkošanas vēsturi">
<!ENTITY  locbar.history2.accesskey     "V">
<!ENTITY  locbar.bookmarks.label        "Grāmatzīmes">
<!ENTITY  locbar.bookmarks.accesskey    "m">
<!ENTITY  locbar.openpage.label         "Atvērtās cilnes">
<!ENTITY  locbar.openpage.accesskey     "t">
<!ENTITY  locbar.searches.label         "Saistītie meklēšanas rezultāti no noklusētā meklētāja">
<!ENTITY  locbar.searches.accesskey     "r">

<!ENTITY  suggestionSettings2.label     "Izmainiet meklētāju iestatījumus">

<!ENTITY  acceptCookies2.label          "Pieņemt sīkdatnes no lapām">
<!ENTITY  acceptCookies2.accesskey      "P">

<!ENTITY  acceptThirdParty2.pre.label     "Pieņemt trešo pušu sīkdatnes">
<!ENTITY  acceptThirdParty2.pre.accesskey "p">
<!ENTITY  acceptThirdParty.always.label   "Vienmēr">
<!ENTITY  acceptThirdParty.never.label    "Nekad">
<!ENTITY  acceptThirdParty.visited.label  "Tikai no lapām, kuras esmu apmeklējis">

<!ENTITY  keepUntil2.label              "Glabāt līdz">
<!ENTITY  keepUntil2.accesskey          "l">

<!ENTITY  expire.label                  "beidzas to derīgums">
<!ENTITY  close.label                   "es aizveru &brandShortName;">

<!ENTITY  cookieExceptions.label        "Izņēmumi…">
<!ENTITY  cookieExceptions.accesskey    "I">

<!ENTITY  showCookies.label             "Parādīt sīkdatnes…">
<!ENTITY  showCookies.accesskey         "s">

<!ENTITY  historyHeader2.pre.label         "&brandShortName;">
<!ENTITY  historyHeader2.pre.accesskey     "v">
<!ENTITY  historyHeader.remember.label     "Atcerēsies vēsturi">
<!ENTITY  historyHeader.dontremember.label "Nekad neatcerēsies vēsturi">
<!ENTITY  historyHeader.custom.label       "Izmantos pielāgotus vēstures iestatījumus">
<!ENTITY  historyHeader.post.label         "">

<!ENTITY  rememberDescription.label      "&brandShortName; atcerēsies jūsu pārlūkošanas, lejupielāžu, aizpildīto formu un meklēšanu vēsturi, kā arī saglabās apmeklēto lapu sīkdatnes.">

<!-- LOCALIZATION NOTE (rememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.middle.label): include a starting and trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.post.label): include a starting space as needed -->
<!ENTITY  rememberActions.pre.label           "Iespējams jūs vēlaties ">
<!ENTITY  rememberActions.clearHistory.label  "dzēst nesenās pārlūkošanas vēsturi">
<!ENTITY  rememberActions.middle.label        " vai ">
<!ENTITY  rememberActions.removeCookies.label "dzēst atsevišķas sīkdatnes">
<!ENTITY  rememberActions.post.label          ".">

<!ENTITY  dontrememberDescription.label  "&brandShortName; izmantos tādus pat iestatījumus kā privātās pārlūkošanas režīmā un pārlūkojot internetu nesaglabās vēsturi.">

<!-- LOCALIZATION NOTE (dontrememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (dontrememberActions.post.label): include a starting space as needed -->
<!ENTITY  dontrememberActions.pre.label          "Jūs varētu vēlēties arī ">
<!ENTITY  dontrememberActions.clearHistory.label "dzēst šobrīd saglabāto vēsturi">
<!ENTITY  dontrememberActions.post.label         ".">

<!ENTITY  privateBrowsingPermanent2.label "Vienmēr izmantot privātās pārlūkošanas režīmu">
<!ENTITY  privateBrowsingPermanent2.accesskey "z">

<!ENTITY  rememberHistory2.label      "Atcerēties manu pārlūkošanas un lejupielāžu vēsturi">
<!ENTITY  rememberHistory2.accesskey  "c">

<!ENTITY  rememberSearchForm.label       "Atcerēties meklēšanas un formu vēsturi">
<!ENTITY  rememberSearchForm.accesskey   "v">

<!ENTITY  clearOnClose.label             "Dzēst aizverot &brandShortName;">
<!ENTITY  clearOnClose.accesskey         "D">

<!ENTITY  clearOnCloseSettings.label     "Iestatījumi…">
<!ENTITY  clearOnCloseSettings.accesskey "t">

<!ENTITY  browserContainersLearnMore.label      "Uzzināt vairāk">
<!ENTITY  browserContainersEnabled.label        "Aktivēt saturošās cilnes">
<!ENTITY  browserContainersEnabled.accesskey    "u">
<!ENTITY  browserContainersSettings.label        "Iestatījumi...">
<!ENTITY  browserContainersSettings.accesskey    "i">

<!ENTITY  a11yPrivacy.checkbox.label     "Neļaut pieejamības rīkiem piekļūt pārlūkam">
<!ENTITY  a11yPrivacy.checkbox.accesskey "p">
<!ENTITY  a11yPrivacy.learnmore.label    "Uzzināt vairāk">
<!ENTITY enableSafeBrowsingLearnMore.label "Uzzināt vairāk">
