<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY  trackingProtectionHeader2.label      "ટ્રેકિંગ પ્રોટેક્શન">
<!ENTITY  trackingProtection2.description      "ટ્રેકિંગ એ બહુવિધ વેબસાઇટ્સ પર તમારા બ્રાઉઝિંગ ડેટાનો સંગ્રહ છે. ટ્રેકિંગનો ઉપયોગ તમારી બ્રાઉઝિંગ અને વ્યક્તિગત માહિતી પર આધારિત રૂપરેખા બનાવવા અને સામગ્રી પ્રદર્શિત કરવા માટે થઈ શકે છે.">
<!ENTITY  trackingProtection2.radioGroupLabel  "જાણીતા ટ્રેકર્સને અવરોધિત કરવા માટે ટ્રેકિંગ પ્રોટેક્શનનો ઉપયોગ કરો">
<!ENTITY  trackingProtection3.description      "ટ્રેકિંગ પ્રોટેક્શન ઓનલાઇન ટ્રેકર જે બહુવિધ વેબસાઇટ્સ પર તમારા બ્રાઉઝિંગ ડેટાને એકત્રિત કરે તેને અવરોધે છે.">
<!ENTITY  trackingProtection3.radioGroupLabel  "જાણીતા ટ્રેકર્સને અવરોધિત કરવા માટે ટ્રેકિંગ પ્રોટેક્શનનો ઉપયોગ કરો">
<!ENTITY  trackingProtectionAlways.label       "હંમેશા">
<!ENTITY  trackingProtectionAlways.accesskey   "y">
<!ENTITY  trackingProtectionPrivate.label      "ખાનગી વિન્ડો માં જ">
<!ENTITY  trackingProtectionPrivate.accesskey  "I">
<!ENTITY  trackingProtectionNever.label        "ક્યારેય નહિં">
<!ENTITY  trackingProtectionNever.accesskey    "n">
<!ENTITY  trackingProtectionLearnMore.label    "વધુ શીખો">
<!ENTITY  trackingProtectionLearnMore2.label    "ટ્રેકિંગ સુરક્ષા અને તમારી ગોપનીયતા વિશે વધુ જાણો">
<!ENTITY  trackingProtectionExceptions.label   "અપવાદો…">
<!ENTITY  trackingProtectionExceptions.accesskey "x">

<!-- LOCALIZATION NOTE (trackingProtectionPBM6.label): This string is displayed if privacy.trackingprotection.ui.enabled is set to false. This currently happens on the release and beta channel. -->
<!ENTITY trackingProtectionPBM6.label         "જાણીતા ટ્રેકર્સને અવરોધિત કરવા માટે ખાનગી બ્રાઉઝિંગમાં ટ્રેકિંગ પ્રોટેક્શનનો ઉપયોગ કરો">
<!ENTITY trackingProtectionPBM6.accesskey     "v">
<!ENTITY trackingProtectionPBMLearnMore.label "વધુ શીખો">
<!ENTITY changeBlockList2.label               "અવરોધિત સૂચિ બદલો…">
<!ENTITY changeBlockList2.accesskey           "C">

<!ENTITY  doNotTrack.description        "વેબસાઇટ્સને &quot;ટ્રેક ન કરો&quot; સિગ્નલ મોકલો કે જેને તમે ટ્રૅક કરી ન શકો">
<!ENTITY  doNotTrack.learnMore.label    "વધુ શીખો">
<!ENTITY  doNotTrack.default.label      "માત્ર ટ્રૅકિંગ પ્રોટેક્શનનો ઉપયોગ કરતી વખતે">
<!ENTITY  doNotTrack.always.label       "હંમેશા">

<!ENTITY  history.label                 "ઇતિહાસ">
<!ENTITY  permissions.label             "પરવાનગીઓ">

<!ENTITY  addressBar.label              "સરનામા પટ્ટી">
<!ENTITY  addressBar.suggest.label      "સરનામાં બારનો ઉપયોગ કરતી વખતે, સૂચન કરો

">
<!ENTITY  locbar.history2.label         "બ્રાઉઝિંગ ઇતિહાસ">
<!ENTITY  locbar.history2.accesskey     "H">
<!ENTITY  locbar.bookmarks.label        "બુકમાર્કો">
<!ENTITY  locbar.bookmarks.accesskey    "k">
<!ENTITY  locbar.openpage.label         "ટૅબ્સ ખોલો">
<!ENTITY  locbar.openpage.accesskey     "O">
<!ENTITY  locbar.searches.label         "મૂળભૂત શોધ એંજીનથી સંબંધિત શોધ">
<!ENTITY  locbar.searches.accesskey     "d">

<!ENTITY  suggestionSettings2.label     "શોધ એન્જિન સૂચનો માટે પસંદગીઓ બદલો">

<!ENTITY  acceptCookies2.label          "વેબસાઇટ્સની કૂકીઝને સ્વીકારો">
<!ENTITY  acceptCookies2.accesskey      "A">

<!ENTITY  acceptThirdParty2.pre.label     "તૃતીય-પક્ષ કૂકીઝ સ્વીકારો">
<!ENTITY  acceptThirdParty2.pre.accesskey "y">

<!ENTITY  acceptCookies3.accesskey      "A">

<!ENTITY  blockCookies.accesskey        "B">

<!ENTITY  acceptThirdParty3.pre.accesskey "y">
<!ENTITY  acceptThirdParty.always.label   "હંમેશા">
<!ENTITY  acceptThirdParty.never.label    "ક્યારેય નહિં">
<!ENTITY  acceptThirdParty.visited.label  "મુલાકાત લીધેલ">

<!ENTITY  keepUntil2.label              "ત્યાં સુધી રાખો">
<!ENTITY  keepUntil2.accesskey          "u">

<!ENTITY  expire.label                  "જ્યાં સુધી સમયસમાપ્ત નહિં થાય">
<!ENTITY  close.label                   "હું &brandShortName; બંધ કરું છું">

<!ENTITY  cookieExceptions.label        "અપવાદો...">
<!ENTITY  cookieExceptions.accesskey    "E">

<!ENTITY  showCookies.label             "કુકીઓ બતાવો...">
<!ENTITY  showCookies.accesskey         "S">

<!ENTITY  historyHeader2.pre.label         "&brandShortName; કરશે">
<!ENTITY  historyHeader2.pre.accesskey     "w">
<!ENTITY  historyHeader.remember.label     "ઇતિહાસ યાદ રાખો">
<!ENTITY  historyHeader.dontremember.label "ક્યારેય ઇતિહાસ યાદ રાખશો નહિં">
<!ENTITY  historyHeader.custom.label       "ઇતિહાસ માટે વૈવિધ્યપૂર્ણ સેટીંગ વાપરો">
<!ENTITY  historyHeader.post.label         "">

<!ENTITY  rememberDescription.label      "&brandShortName; તમે બ્રાઉઝ કરતાં જે વેબ સાઉટોની મુલાકાત લો તેમાંથી, ડાઉનલોડ, ફોર્મ અને શોધ ઇતિહાસ, અને કુકીઓને યાદ રાખશે.">

<!-- LOCALIZATION NOTE (rememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.middle.label): include a starting and trailing space as needed -->
<!-- LOCALIZATION NOTE (rememberActions.post.label): include a starting space as needed -->
<!ENTITY  rememberActions.pre.label           "તમે ">
<!ENTITY  rememberActions.clearHistory.label  "તમારો છેલ્લે ઇતિહાસ સાફ કરવા">
<!ENTITY  rememberActions.middle.label        ", અથવા ">
<!ENTITY  rememberActions.removeCookies.label "વ્યક્તિગત કુકીઓ દૂર કરવા માંગતા હશો">
<!ENTITY  rememberActions.post.label          ".">

<!ENTITY  dontrememberDescription.label  "&brandShortName; ખાનગી બ્રાઉઝીંગની જેમ જ સેટીંગ વાપરશે, અને તમે જેમ વેબ બ્રાઉઝ કરો તેમ ઇતિહાસ યાદ રાખશે નહિં.">

<!-- LOCALIZATION NOTE (dontrememberActions.pre.label): include a trailing space as needed -->
<!-- LOCALIZATION NOTE (dontrememberActions.post.label): include a starting space as needed -->
<!ENTITY  dontrememberActions.pre.label          "તમે ">
<!ENTITY  dontrememberActions.clearHistory.label "બધો વર્તમાન ઇતિહાસ પણ સાફ કરવા માંગતા હશો">
<!ENTITY  dontrememberActions.post.label         ".">

<!ENTITY  clearHistoryButton.label       "ઇતિહાસ સાફ કરો…">
<!ENTITY  clearHistoryButton.accesskey   "s">

<!ENTITY  privateBrowsingPermanent2.label "હંમેશા ખાનગી બ્રાઉઝીંગ સ્થિતિ વાપરો">
<!ENTITY  privateBrowsingPermanent2.accesskey "p">

<!ENTITY  rememberHistory2.label      "મારુ બ્રાઉઝીંગ અને ડાઉનલોડ ઇતિહાસને યાદ રાખો">
<!ENTITY  rememberHistory2.accesskey  "b">

<!ENTITY  rememberSearchForm.label       "શોધ અને ફોર્મ ઇતિહાસ યાદ રાખો">
<!ENTITY  rememberSearchForm.accesskey   "f">

<!ENTITY  clearOnClose.label             "જ્યારે &brandShortName; બંધ થાય ત્યારે ઇતિહાસ સાફ કરો">
<!ENTITY  clearOnClose.accesskey         "r">

<!ENTITY  clearOnCloseSettings.label     "સેટીંગ…">
<!ENTITY  clearOnCloseSettings.accesskey "t">

<!ENTITY  browserContainersLearnMore.label      "વધુ શીખો">
<!ENTITY  browserContainersEnabled.label        "કન્ટેઈનર ટેબ્સ સક્ષમ કરો">
<!ENTITY  browserContainersEnabled.accesskey    "n">
<!ENTITY  browserContainersSettings.label        "સેટીંગ…">
<!ENTITY  browserContainersSettings.accesskey    "i">

<!ENTITY  a11yPrivacy.checkbox.label     "ઍક્સેસિબિલિટી સેવાઓને તમારા બ્રાઉઝરને ઍક્સેસ કરવાથી અટકાવો">
<!ENTITY  a11yPrivacy.checkbox.accesskey "a">
<!ENTITY  a11yPrivacy.learnmore.label    "વધુ શીખો">
<!ENTITY enableSafeBrowsingLearnMore.label "વધુ શીખો">
