# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

do-not-track-description = Thumelela iiwebhsayithi umqondiso othi "Musa ukuTreka" obonisa ukuba akufuni kutrekwa
do-not-track-learn-more = Funda ngakumbi
do-not-track-option-default =
    .label = Kuphela xa usebenzisa uKhuseleko lokuTreka
do-not-track-option-always =
    .label = Qho
pref-page =
    .title = { PLATFORM() ->
            [windows] Ekunokukhethwa kuko
           *[other] Izikhethwa
        }
pane-general-title = Jikelele
category-general =
    .tooltiptext = { pane-general-title }
pane-search-title = Khangela
category-search =
    .tooltiptext = { pane-search-title }
pane-privacy-title = UbuNgasese noKhuseleko
category-privacy =
    .tooltiptext = { pane-privacy-title }
# The word "account" can be translated, do not translate or transliterate "Firefox".
pane-sync-title = IAkhawunti yeFirefox
category-sync =
    .tooltiptext = { pane-sync-title }
help-button-label = INkxaso ye{ -brand-short-name }
focus-search =
    .key = f
close-button =
    .aria-label = Vala

## Browser Restart Dialog

feature-enable-requires-restart = I-{ -brand-short-name } kufuneka iqalise ngokutsha ukuyenza isebenze ifitsha.
feature-disable-requires-restart = I-{ -brand-short-name } kufuneka iqalise ngokutsha ukuyenza ingasebenzi le fitsha.
should-restart-title = Qalisa kwakhona { -brand-short-name }
should-restart-ok = Phinda uqale i{ -brand-short-name } ngoku
revert-no-restart-button = Buyela kweyangaphambili
restart-later = Qalisa ngokutsha Kamva
