<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY restorepage.tabtitle       "Restaurar sessão">

<!-- LOCALIZATION NOTE: The title is intended to be apologetic and disarming, expressing dismay
     and regret that we are unable to restore the session for the user -->
<!ENTITY restorepage.errorTitle2    "Lamentamos. Estamos a ter problemas para obter as suas páginas de volta.">
<!ENTITY restorepage.problemDesc2   "Estamos a ter problemas para restaurar a sua última sessão de navegação. Selecione Restaurar sessão para tentar novamente.">
<!ENTITY restorepage.tryThis2       "Ainda não consegue restaurar a sua sessão? Por vezes, um separador pode estar a causar o problema. Veja os separadores anteriores, remova a marca de verificação dos separadores que não precisa de recuperar, e depois restaure.">

<!ENTITY restorepage.hideTabs       "Ocultar separadores anteriores">
<!ENTITY restorepage.showTabs       "Ver separadores anteriores">

<!ENTITY restorepage.tryagainButton2 "Restaurar sessão">
<!ENTITY restorepage.restore.access2 "R">
<!ENTITY restorepage.closeButton2    "Iniciar nova sessão">
<!ENTITY restorepage.close.access2   "n">

<!ENTITY restorepage.restoreHeader  "Restaurar">
<!ENTITY restorepage.listHeader     "Janelas e separadores">
<!-- LOCALIZATION NOTE: &#37;S will be replaced with a number. -->
<!ENTITY restorepage.windowLabel    "Janela &#37;S">


<!-- LOCALIZATION NOTE: The following 'welcomeback2' strings are for about:welcomeback,
     not for about:sessionstore -->

<!ENTITY welcomeback2.restoreButton  "Vamos lá!">
<!ENTITY welcomeback2.restoreButton.access "L">

<!ENTITY welcomeback2.tabtitle      "Sucesso!">

<!ENTITY welcomeback2.pageTitle     "Sucesso!">
<!ENTITY welcomeback2.pageInfo1     "O &brandShortName; está pronto.">

<!ENTITY welcomeback2.restoreAll.label  "Restaurar todas as janelas e separadores">
<!ENTITY welcomeback2.restoreSome.label "Restaurar apenas aqueles que deseja">


<!-- LOCALIZATION NOTE (welcomeback2.beforelink.pageInfo2,
welcomeback2.afterlink.pageInfo2): these two string are used respectively
before and after the the "learn more" link (welcomeback2.link.pageInfo2).
Localizers can use one of them, or both, to better adapt this sentence to
their language.
-->
<!ENTITY welcomeback2.beforelink.pageInfo2  "Os seus extras e personalizações foram removidos e as definições do seu navegador foram restauradas para as suas predefinições. Se isto não corrigiu o seu problema, ">
<!ENTITY welcomeback2.afterlink.pageInfo2   "">

<!ENTITY welcomeback2.link.pageInfo2        "saiba mais sobre o que pode fazer.">

