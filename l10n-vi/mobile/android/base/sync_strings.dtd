<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Đồng bộ Firefox">
<!ENTITY syncBrand.shortName.label "Đồng bộ">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Kết nối đến &syncBrand.shortName.label;'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Để kích hoạt thiết bị mới của bạn, hãy chọn \&quot;&syncBrand.shortName.label;\&quot; trên thiết bị.'>
<!ENTITY sync.subtitle.pair.label 'Để kích hoạt, chọn “Ghép đôi thiết bị” trên thiết bị khác của bạn.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Tôi không có thiết bị…'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Đăng nhập'>
<!ENTITY sync.configure.engines.title.history 'Lược sử'>
<!ENTITY sync.configure.engines.title.tabs 'Thẻ'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; trên &formatS2;'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Trình đơn Đánh dấu'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Nhãn'>
<!ENTITY bookmarks.folder.toolbar.label 'Thanh Đánh Dấu'>
<!ENTITY bookmarks.folder.other.label 'Trang đánh dấu khác'>
<!ENTITY bookmarks.folder.desktop.label 'Trang đánh dấu Desktop'>
<!ENTITY bookmarks.folder.mobile.label 'Trang đánh dấu Di động'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Đã ghim'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Trở về trình duyệt'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Chào mừng đến với &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Đăng nhập để đồng bộ thẻ, trang đánh dấu, mật khẩu &amp; các phần khác.'>
<!ENTITY fxaccount_getting_started_get_started 'Bắt đầu'>
<!ENTITY fxaccount_getting_started_old_firefox 'Bạn đang sử dụng phiên bản cũ của &syncBrand.shortName.label;?'>

<!ENTITY fxaccount_status_auth_server 'Máy chủ tài khoản'>
<!ENTITY fxaccount_status_sync_now 'Đồng bộ ngay'>
<!ENTITY fxaccount_status_syncing2 'Đang đồng bộ...'>
<!ENTITY fxaccount_status_device_name 'Tên thiết bị'>
<!ENTITY fxaccount_status_sync_server 'Máy chủ đồng bộ'>
<!ENTITY fxaccount_status_needs_verification2 'Tài khoản của bạn cần được xác thực. Chạm để gửi lại thư xác thực.'>
<!ENTITY fxaccount_status_needs_credentials 'Không thể kết nối. Chạm để đăng nhập.'>
<!ENTITY fxaccount_status_needs_upgrade 'Bạn cần cập nhật &brandShortName; để đăng nhập.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; đã được thiết lập, nhưng không làm việc tự động. Bật “Tự động đồng bộ dữ liệu” trong Cài đặt Android &gt; Sử dụng dữ liệu.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; đã được thiết lập nhưng không làm việc tự động. Bật “Tự động đồng bộ dữ liệu” trong Cài đặt Android &gt; Tài khoản.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Chạm để đăng nhập tài khoản Firefox mới.'>
<!ENTITY fxaccount_status_choose_what 'Chọn những gì bạn muốn đồng bộ hóa'>
<!ENTITY fxaccount_status_bookmarks 'Trang đánh dấu'>
<!ENTITY fxaccount_status_history 'Lược sử'>
<!ENTITY fxaccount_status_passwords2 'Đăng nhập'>
<!ENTITY fxaccount_status_tabs 'Các thẻ đang mở'>
<!ENTITY fxaccount_status_additional_settings 'Cài đặt bổ sung'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Chỉ đồng bộ qua Wi-Fi'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Ngăn chặn &brandShortName; đồng bộ hóa thông qua mạng di động hoặc mạng đo lường'>
<!ENTITY fxaccount_status_legal 'Hợp pháp' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Điều khoản dịch vụ'>
<!ENTITY fxaccount_status_linkprivacy2 'Chính sách riêng tư'>
<!ENTITY fxaccount_remove_account 'Ngưng kết nối&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Ngắt kết nối đến Sync?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Dữ liệu duyệt web của bạn sẽ vẫn trên thiết bị này, nhưng nó sẽ không còn đồng bộ với tài khoản của bạn.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Tài khoản Firefox &formatS; ngắt kết nối.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Ngắt kết nối'>

<!ENTITY fxaccount_enable_debug_mode 'Bật chế độ gỡ lỗi'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; Tùy chọn'>
<!ENTITY fxaccount_options_configure_title 'Cài đặt &syncBrand.shortName.label;'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; không được kết nối'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Chạm để đăng nhập tài khoản &formatS;'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Kết thúc nâng cấp &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Chạm để đăng nhập với tài khoản &formatS;'>
