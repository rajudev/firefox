# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

do-not-track-learn-more = ఇంకా తెలుసుకోండి
do-not-track-option-default =
    .label = ట్రాకింగ్ సంరక్షణ వాడుతున్నప్పుడు మాత్రమే
do-not-track-option-always =
    .label = ఎల్లప్పుడూ
pref-page =
    .title = { PLATFORM() ->
            [windows] ఎంపికలు
           *[other] అభిరుచులు
        }
# This is used to determine the width of the search field in about:preferences,
# in order to make the entire placeholder string visible
#
# Notice: The value of the `.style` attribute is a CSS string, and the `width`
# is the name of the CSS property. It is intended only to adjust the element's width.
# Do not translate.
search-input =
    .style = width: 15.4em
pane-general-title = సాధారణం
category-general =
    .tooltiptext = { pane-general-title }
pane-search-title = వెతకడం
category-search =
    .tooltiptext = { pane-search-title }
pane-privacy-title = అంతరంగికత & భద్రత
category-privacy =
    .tooltiptext = { pane-privacy-title }
# The word "account" can be translated, do not translate or transliterate "Firefox".
pane-sync-title = Firefox ఖాతా
category-sync =
    .tooltiptext = { pane-sync-title }
help-button-label = { -brand-short-name } తోడ్పాటు
focus-search =
    .key = f
close-button =
    .aria-label = మూసివేయి

## Browser Restart Dialog

feature-enable-requires-restart = ఈ విశేషణం చేతనం చేయుటకు { -brand-short-name } ను తప్పక పునఃప్రారంభించాలి.
feature-disable-requires-restart = ఈ విశేషణం అచేతనం చేయుటకు { -brand-short-name } ను తప్పక పునఃప్రారంభించాలి.
should-restart-title = { -brand-short-name } పునఃప్రారంభించు
should-restart-ok = ఇప్పుడు { -brand-short-name } ను పునఃప్రారంభించు
revert-no-restart-button = రద్దుచేయి
restart-later = తరువాత పునఃప్రారంభించు
