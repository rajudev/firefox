<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "फायरफाक्स सिंक">
<!ENTITY syncBrand.shortName.label "सिंक">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label '&syncBrand.shortName.label; कनेक्ट करू…'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'अपन नई युक्ति केँ सक्रिय करब कलेल, “Set up &syncBrand.shortName.label;” केँ एहि युक्ति पर चुनू.'>
<!ENTITY sync.subtitle.pair.label 'सक्रिय करब कलेल, अपन आन डिवाइस पर \&quot;एकटा युक्ति जोड़बाक\&quot; केँ चुनू.'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'हमरा संग युक्ति नहि अछि'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'लॉगिन'>
<!ENTITY sync.configure.engines.title.history 'इतिहास'>
<!ENTITY sync.configure.engines.title.tabs 'टैग'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1; &formatS2; पर'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'पुस्तकचिह्न  मेनू '>
<!ENTITY bookmarks.folder.places.label 'टैग'>
<!ENTITY bookmarks.folder.tags.label 'टैग'>
<!ENTITY bookmarks.folder.toolbar.label 'पुस्तकचिह्न अओजारपट्टी'>
<!ENTITY bookmarks.folder.other.label 'आन पुस्तचिह्न'>
<!ENTITY bookmarks.folder.desktop.label 'डेस्कटाप पुस्तकचिह्न'>
<!ENTITY bookmarks.folder.mobile.label 'Mobile Bookmarks'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'टांगल'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'ब्राउजिंग मे वापस'>

<!ENTITY fxaccount_getting_started_welcome_to_sync '&syncBrand.shortName.label; मे अहाँक स्वागत अछि ;'>
<!ENTITY fxaccount_getting_started_description2 'टैब, पुस्तकचिह्न, लॉगिन आदि सिंक करब कलेल साइन इन करू.'>
<!ENTITY fxaccount_getting_started_get_started 'आरंभ करू'>
<!ENTITY fxaccount_getting_started_old_firefox '&syncBrand.shortName.label; क पुरान संस्करण क उपयोग कए रहल अछि?'>

<!ENTITY fxaccount_status_auth_server 'खाताक सर्वर'>
<!ENTITY fxaccount_status_sync_now 'तुल्यकालन एखन करु'>
<!ENTITY fxaccount_status_syncing2 'तुल्यकालन भए रहल अछि'>
<!ENTITY fxaccount_status_device_name 'युक्तिक नाम'>
<!ENTITY fxaccount_status_sync_server 'सिंक सर्वर'>
<!ENTITY fxaccount_status_needs_verification2 'अहाँक खाता केँ सत्यापित करबाक जरूरत अछि. दोबारा सत्यापन इमेल पठेबाक लेल टाइप करु.'>
<!ENTITY fxaccount_status_needs_credentials 'कनेक्ट नहि कए सकल. साइन-इन करबाक लेल टाइप करु.'>
<!ENTITY fxaccount_status_needs_upgrade 'साइन-इन करबाक लेल अहाँकेँ &brandShortName; अद्यतन करबाक जरूरत अछि.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label; सेटअप कएल गेल अछि, मुदा स्वतः तुल्यकालन नहि होएत अछि. “डाटा स्वतःतुल्यकालन” एंड्रायड सेटिंग में टॉगल करु &gt; डाटा प्रयोग.'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label; सेटअप कएल गेल अछि, मुदा स्वतः सिंक नहि हाएत अछि. “डाटा स्वतःतुल्यकालन” एंड्राइड सेटिंग मे टॉगल करु &gt; डाटा प्रयोग.'>
<!ENTITY fxaccount_status_needs_finish_migrating 'अपन नवका फायरफॉक्स खाता में साइन इन करबाक लेल टीपू.'>
<!ENTITY fxaccount_status_choose_what 'चुनू जे सिंक कएनाय अछि'>
<!ENTITY fxaccount_status_bookmarks 'पुस्तचिह्न'>
<!ENTITY fxaccount_status_history 'इतिहास'>
<!ENTITY fxaccount_status_passwords2 'लॉगिन'>
<!ENTITY fxaccount_status_tabs 'टैब खोलू'>
<!ENTITY fxaccount_status_additional_settings 'अतिरिक्त सेटिंग'>
<!ENTITY fxaccount_pref_sync_use_metered2 'केवल वाई-फ़ाई पर सिंक करू'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 '&brandShortName; केँ कोनो सेल्युलर या मीटरिंग नेटवर्क पर सिंक हए सँ रोकू'>
<!ENTITY fxaccount_status_legal 'कानूनी' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'सेवा क शर्त'>
<!ENTITY fxaccount_status_linkprivacy2 'गोपनीयता सूचना'>
<!ENTITY fxaccount_remove_account '&ellipsis;डिसकनेक्ट करू'>

<!ENTITY fxaccount_remove_account_dialog_title2 'सिंक सँ संपर्क हटाएब?'>
<!ENTITY fxaccount_remove_account_dialog_message2 'अहाँक ब्राउज़िंग डेटा ई उपकरण पर रहत, मुदा ई आब अहाँक खाता सँग सिंक नहि हाएत.'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Firefox खाता &formatS; असंबद्ध.'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'कनेक्शन हटाबू'>

<!ENTITY fxaccount_enable_debug_mode 'डिबग मोड सक्रिय करू'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'फ़ायरफ़ॉक्स'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label; विकल्प'>
<!ENTITY fxaccount_options_configure_title '&syncBrand.shortName.label; कॉन्फिगर करू'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label; कनेक्टेड नहि अछि'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 ' &formatS; रूपेँ साइन-इन करब कलेल टैप करू'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'अपग्रेडिंग समाप्त करू &syncBrand.shortName.label;?'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text '&formatS; रूपेँ साइन-इन करब कलेल टैप करू'>
