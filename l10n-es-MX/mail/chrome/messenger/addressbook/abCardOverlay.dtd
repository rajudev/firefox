<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY Contact.tab                     "Contacto">
<!ENTITY Contact.accesskey               "C">
<!ENTITY Name.box                        "Nombre">

<!-- LOCALIZATION NOTE:
 NameField1, NameField2, PhoneticField1, PhoneticField2
 those fields are either LN or FN depends on the target country.
 "FirstName" and "LastName" can be swapped for id to change the order
 but they should not be translated (same applied to phonetic id).
 Make sure the translation of label corresponds to the order of id.
-->

<!-- LOCALIZATION NOTE (NameField1.id) : DONT_TRANSLATE -->
<!ENTITY NameField1.id                  "FirstName">
<!-- LOCALIZATION NOTE (NameField2.id) : DONT_TRANSLATE -->
<!ENTITY NameField2.id                  "LastName">
<!-- LOCALIZATION NOTE (PhoneticField1.id) : DONT_TRANSLATE -->
<!ENTITY PhoneticField1.id              "PhoneticFirstName">
<!-- LOCALIZATION NOTE (PhoneticField2.id) : DONT_TRANSLATE -->
<!ENTITY PhoneticField2.id              "PhoneticLastName">

<!ENTITY NameField1.label               "Nombre:">
<!ENTITY NameField1.accesskey           "F">
<!ENTITY NameField2.label               "Apellido:">
<!ENTITY NameField2.accesskey           "L">
<!ENTITY PhoneticField1.label           "Fonética:">
<!ENTITY PhoneticField2.label           "Fonética:">
<!ENTITY DisplayName.label              "Mostrar:">
<!ENTITY DisplayName.accesskey          "D">
<!ENTITY preferDisplayName.label        "Preferir siempre el nombre mostrado en lugar del encabezado del mensaje">
<!ENTITY preferDisplayName.accesskey    "y">
<!ENTITY NickName.label                 "Apodo:">
<!ENTITY NickName.accesskey             "N">

<!ENTITY PrimaryEmail.label             "Correo electrónico:">
<!ENTITY PrimaryEmail.accesskey         "E">
<!ENTITY SecondEmail.label              "Correo electrónico adicional:">
<!ENTITY SecondEmail.accesskey          "i">
<!ENTITY PreferMailFormat.label         "Prefiere recibir mensajes formateados como:">
<!ENTITY PreferMailFormat.accesskey     "v">
<!ENTITY PlainText.label                "Texto simple">
<!ENTITY HTML.label                     "HTML">
<!ENTITY Unknown.label                  "Desconocido">
<!ENTITY chatName.label                 "Nombre del chat:">

<!ENTITY WorkPhone.label                "Trabajo:">
<!ENTITY WorkPhone.accesskey            "k">
<!ENTITY HomePhone.label                "Inicio:">
<!ENTITY HomePhone.accesskey            "m">
<!ENTITY FaxNumber.label                "Fax:">
<!ENTITY FaxNumber.accesskey            "x">
<!ENTITY PagerNumber.label              "Buscapersonas:">
<!ENTITY PagerNumber.accesskey          "g">
<!ENTITY CellularNumber.label           "Celular:">
<!ENTITY CellularNumber.accesskey       "b">

<!ENTITY Home.tab                       "Privado">
<!ENTITY Home.accesskey                 "P">
<!ENTITY HomeAddress.label              "Dirección:">
<!ENTITY HomeAddress.accesskey          "d">
<!ENTITY HomeAddress2.label             "">
<!ENTITY HomeAddress2.accesskey         "">
<!ENTITY HomeCity.label                 "Ciudad:">
<!ENTITY HomeCity.accesskey             "y">
<!ENTITY HomeState.label                "Estado/Provincia:">
<!ENTITY HomeState.accesskey            "S">
<!ENTITY HomeZipCode.label              "Código postal:">
<!ENTITY HomeZipCode.accesskey          "Z">
<!ENTITY HomeCountry.label              "País:">
<!ENTITY HomeCountry.accesskey          "u">
<!ENTITY HomeWebPage.label              "Página web:">
<!ENTITY HomeWebPage.accesskey          "e">
<!ENTITY Birthday.label                 "Cumpleaños:">
<!ENTITY Birthday.accesskey             "B">
<!ENTITY In.label                       "">
<!ENTITY Year.placeholder               "Año">
<!ENTITY Or.value                       "o">
<!ENTITY Age.placeholder                "Edad">
<!ENTITY YearsOld.label                 "">

<!ENTITY Work.tab                       "Trabajo">
<!ENTITY Work.accesskey                 "W">
<!ENTITY JobTitle.label                 "Título:">
<!ENTITY JobTitle.accesskey             "i">
<!ENTITY Department.label               "Departamento:">
<!ENTITY Department.accesskey           "m">
<!ENTITY Company.label                  "Organización:">
<!ENTITY Company.accesskey              "n">
<!ENTITY WorkAddress.label              "Dirección:">
<!ENTITY WorkAddress.accesskey          "d">
<!ENTITY WorkAddress2.label             "">
<!ENTITY WorkAddress2.accesskey         "">
<!ENTITY WorkCity.label                 "Ciudad:">
<!ENTITY WorkCity.accesskey             "y">
<!ENTITY WorkState.label                "Estado/Provincia:">
<!ENTITY WorkState.accesskey            "S">
<!ENTITY WorkZipCode.label              "Código postal:">
<!ENTITY WorkZipCode.accesskey          "Z">
<!ENTITY WorkCountry.label              "País:">
<!ENTITY WorkCountry.accesskey          "u">
<!ENTITY WorkWebPage.label              "Página web:">
<!ENTITY WorkWebPage.accesskey          "e">

<!ENTITY Other.tab                      "Otro">
<!ENTITY Other.accesskey                "h">
<!ENTITY Custom1.label                  "Personalizado 1:">
<!ENTITY Custom1.accesskey              "1">
<!ENTITY Custom2.label                  "Personalizado 2:">
<!ENTITY Custom2.accesskey              "2">
<!ENTITY Custom3.label                  "Personalizado 3:">
<!ENTITY Custom3.accesskey              "3">
<!ENTITY Custom4.label                  "Personalizado 4:">
<!ENTITY Custom4.accesskey              "4">
<!ENTITY Notes.label                    "Notas:">
<!ENTITY Notes.accesskey                "N">

<!ENTITY Chat.tab                       "Chat">
<!ENTITY Chat.accesskey                 "a">
<!ENTITY Gtalk.label                    "Google Talk:">
<!ENTITY Gtalk.accesskey                "G">
<!ENTITY AIM.label                      "AIM:">
<!ENTITY AIM2.accesskey                 "M">
<!ENTITY Yahoo.label                    "Yahoo!:">
<!ENTITY Yahoo.accesskey                "Y">
<!ENTITY Skype.label                    "Skype:">
<!ENTITY Skype.accesskey                "S">
<!ENTITY QQ.label                       "QQ:">
<!ENTITY QQ.accesskey                   "Q">
<!ENTITY MSN.label                      "MSN:">
<!ENTITY MSN2.accesskey                 "N">
<!ENTITY ICQ.label                      "ICQ:">
<!ENTITY ICQ.accesskey                  "I">
<!ENTITY XMPP.label                     "Jabber ID:">
<!ENTITY XMPP.accesskey                 "J">
<!ENTITY IRC.label                      "IRC Nick:">
<!ENTITY IRC.accesskey                  "R">

<!ENTITY Photo.tab                      "Foto">
<!ENTITY Photo.accesskey                "o">
<!ENTITY PhotoDesc.label                "Elige uno de los siguientes:">
<!ENTITY GenericPhoto.label             "Foto genérica">
<!ENTITY GenericPhoto.accesskey         "G">
<!ENTITY DefaultPhoto.label             "Predeterminada">
<!ENTITY PhotoFile.label                "En este equipo">
<!ENTITY PhotoFile.accesskey            "n">
<!ENTITY BrowsePhoto.label              "Navegar">
<!ENTITY BrowsePhoto.accesskey          "r">
<!ENTITY PhotoURL.label                 "En la Web">
<!ENTITY PhotoURL.accesskey             "b">
<!ENTITY PhotoURL.placeholder           "Pega o escribe la dirección web de una foto">
<!ENTITY UpdatePhoto.label              "Actualizar">
<!ENTITY UpdatePhoto.accesskey          "u">
